-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 29, 2018 at 03:10 PM
-- Server version: 10.0.34-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `tpoint`
--

--
-- Dumping data for table `matches`
--

INSERT INTO `matches` (`id`, `score`, `winner`, `location`, `match_dtime`, `last_edit`, `score_added`, `score_conf`) VALUES
(1, '2-6;3-6', 2, '', '2018-07-24 00:00:00', '2018-07-26 16:09:39', '0000-00-00 00:00:00', 0),
(2, '3-6;6-4;6-1', 1, '', '2018-07-22 07:00:00', '2018-07-26 16:09:37', '0000-00-00 00:00:00', 0),
(3, '4-6,7-5,4-6,2-6', 2, '', '2018-07-18 09:00:00', '2018-07-26 16:09:35', '0000-00-00 00:00:00', 0),
(4, '6-7;7-5;7-5', 1, '', '2018-07-25 13:00:00', '2018-07-26 16:09:29', '0000-00-00 00:00:00', 0),
(5, '', 0, 'TC Mitte, Melchiorstr. Berlin', '2018-08-17 18:00:00', '2018-07-29 12:34:03', '0000-00-00 00:00:00', 0),
(6, '', 0, 'TC Wilmersdorf, Cunostr. 28, 14199 Berlin', '2018-08-08 12:00:00', '2018-07-29 12:34:03', '0000-00-00 00:00:00', 0),
(7, '', 0, 'TC Mitte, Melchiorstr. Berlin', '2018-08-14 18:00:00', '2018-07-29 12:34:03', '0000-00-00 00:00:00', 0),
(8, '', 0, 'TC Wilmersdorf, Cunostr. 28, 14199 Berlin', '2018-08-15 12:00:00', '2018-07-29 12:34:03', '0000-00-00 00:00:00', 0);

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `user_id_writer`, `u_character`, `u_technique`, `review_text`, `time_edit`) VALUES
(1, '60bh8f8mjcig', '1cieahglcmr1', 2, 3, NULL, '2018-07-26 16:51:25'),
(2, '60bh8f8mjcig', '1vd2ioq8llot', 3, 5, NULL, '2018-07-26 16:51:25'),
(3, '60bh8f8mjcig', '1pyzwnq9pwbb', 2, 5, NULL, '2018-07-26 16:51:25'),
(4, '1pyzwnq9pwbb', '60bh8f8mjcig', 3, 2, NULL, '2018-07-26 16:51:25');

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `phone`, `show_email`, `show_phone`, `photo`, `gender`, `age`, `city`, `country`, `level`, `dominant_hand`, `backhand_style`, `play_times`, `play_years`, `gear_1`, `main_skill`, `main_skill2`, `pass`, `confirmed`, `last_login`, `time_edit`) VALUES
('1vd2ioq8llot', 'Andre', 'Agassi', 'andre@mail.xer', NULL, 0, 0, NULL, 0, 0, 'cc', 'Canada', 3, 0, 0, 0, 0, NULL, 0, 0, '$2y$12$Yp2.Ml8x8yRyq3CoR.Obf.C5v/nWJe6KySat5FviifyfYOFDg62hy', 0, '2018-07-27 18:19:24', '2018-12-24 15:31:47'),
('60bh8f8mjcig', 'Boris', 'Becker', 'boris@mail.x', NULL, 1, 0, NULL, 1, 33, 'Berlin', 'Germany', 33, 1, 0, 2, 5, 'Scorpion Fin2', 2, 0, '$2y$12$iTvkDo6PmuMJJyEqXVv0su/ZY1Nj6shcfDvOABgG8VR39VvH1HFzS', 0, '2018-07-27 18:19:24', '2018-12-24 15:31:33'),
('1pyzwnq9pwbb', 'Steffi', 'Graf', 'graf@mail.de', NULL, 0, 0, NULL, 0, 0, 'Ottawa', 'Canada', 3, 0, 0, 0, 0, NULL, 0, 0, '$2y$12$iTvkDo6PmuMJJyEqXVv0su/ZY1Nj6shcfDvOABgG8VR39VvH1HFzS', 0, '2018-07-27 18:19:24', '2018-12-24 15:31:28'),
('13lnrsg1m1od', 'Novak', 'Djokovic', 'novak.mr@gmail.hr', NULL, 1, 0, NULL, 1, 31, 'Monte Carlo', 'Monte Carlo', 70, 1, 0, 0, 15, 'Head Graphene Touch Speed Pro', 0, 0, '$2y$12$qH.PqEqBPnrNfnTm1gokFu7a/svoXBj/PuSa81za8xJDGlf2xcmfq', 0, '0000-00-00 00:00:00', '2018-07-29 10:32:44'),
('1cieahglcmr1', 'Simona', 'Halep', 'simona@halep.ro', NULL, 0, 0, NULL, 0, 26, 'Constanta', 'Romania', 45, 0, 0, 7, 12, NULL, 0, 0, '$2y$12$FaZxuETuX0tSjd51X9D2luyrMqNJlDxPxSSS6sUlblk5dMdaRmLBm', 0, '2018-07-29 14:26:46', '2018-07-29 10:26:46');
--
-- Dumping data for table `users_matches`
--

INSERT INTO `users_matches` (`user_id`, `match_id`, `team`) VALUES
('60bh8f8mjcig', 1, 1),
('1pyzwnq9pwbb', 1, 2),
('60bh8f8mjcig', 2, 1),
('1vd2ioq8llot', 2, 2),
('1cieahglcmr1', 3, 1),
('60bh8f8mjcig', 3, 2),
('60bh8f8mjcig', 4, 1),
('1pyzwnq9pwbb', 4, 2),
('1vd2ioq8llot', 5, 1),
('60bh8f8mjcig', 5, 2),
('1pyzwnq9pwbb', 6, 1),
('1cieahglcmr1', 6, 2),
('1pyzwnq9pwbb', 7, 1),
('60bh8f8mjcig', 7, 2),
('1vd2ioq8llot', 7, 1),
('60bh8f8mjcig', 7, 2),
('1cieahglcmr1', 8, 2),
('60bh8f8mjcig', 8, 2);
