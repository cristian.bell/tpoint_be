--29.07
ALTER TABLE `matches` CHANGE `score_date` `last_edit` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `matches` CHANGE `match_datetime` `match_dtime` DATETIME NOT NULL;
ALTER TABLE `matches` ADD `score_conf` BOOLEAN NULL DEFAULT FALSE COMMENT 'if this is 1, no further edits allowed' AFTER `last_edit`;

--27.07
CREATE TABLE IF NOT EXISTS `sessions` (
        `id` varchar(128) NOT NULL,
        `ip_address` varchar(45) NOT NULL,
        `timestamp` int(10) unsigned DEFAULT 0 NOT NULL,
        `data` blob NOT NULL,
        KEY `sessions_timestamp` (`timestamp`)
);
ALTER TABLE sessions ADD PRIMARY KEY (id, ip_address);
ALTER TABLE `users`
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `id` (`id`);


--26.07
ALTER TABLE `matches` CHANGE `played_on` `score_date` TIMESTAMP NOT NULL;
ALTER TABLE `matches` ADD `match_datetime` DATETIME NOT NULL AFTER `winner`;
ALTER TABLE `matches` ADD `location` VARCHAR(100) NOT NULL AFTER `winner`;

--24.07:
ALTER TABLE `users` ADD `photo` VARCHAR(255) NULL AFTER `email`;
ALTER TABLE `users` ADD `phone` VARCHAR(50) NULL AFTER `email`;
ALTER TABLE `users` ADD `show_email` BOOLEAN NOT NULL DEFAULT FALSE AFTER `phone`, ADD `show_phone` BOOLEAN NOT NULL DEFAULT FALSE AFTER `show_email`;
ALTER TABLE `users` ADD `gear_1` VARCHAR(100) NULL AFTER `play_years`;
ALTER TABLE `users` ADD `main_skill` TINYINT(1) UNSIGNED NOT NULL AFTER `gear_1`;
ALTER TABLE `users` CHANGE `dob` `age` TINYINT(2) UNSIGNED NOT NULL;
ALTER TABLE `users` ADD `time_edit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;