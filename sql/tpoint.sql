--
-- Table structure for table `matches`
--

CREATE TABLE `matches` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `score` varchar(40) NOT NULL,
  `winner` tinyint(1) DEFAULT NULL COMMENT '0 - not played or not recorded, 1 or 2 - valid winner',
  `location` varchar(100) NOT NULL,
  `match_dtime` datetime NOT NULL,
  `last_edit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `score_added` datetime NOT NULL,
  `score_conf` tinyint(1) DEFAULT '0' COMMENT 'if this is 1, no further edits allowed'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `user_id` varchar(16) NOT NULL,
  `user_id_writer` varchar(16) NOT NULL COMMENT 'id of user who writes the review',
  `u_character` tinyint(2) UNSIGNED NOT NULL,
  `u_technique` tinyint(2) UNSIGNED NOT NULL,
  `review_text` text,
  `time_edit` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tournaments`
--

CREATE TABLE `tournaments` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `singles` tinyint(1) UNSIGNED NOT NULL,
  `gender` tinyint(1) UNSIGNED NOT NULL,
  `level` float UNSIGNED NOT NULL,
  `elim_rules` tinyint(1) UNSIGNED NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `courts` tinyint(1) UNSIGNED NOT NULL,
  `g_lon` decimal(10,0) NOT NULL,
  `g_lat` decimal(10,0) NOT NULL,
  `sets` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(16) NOT NULL,
  `firstname` varchar(160) NOT NULL,
  `lastname` varchar(160) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `show_email` tinyint(1) NOT NULL DEFAULT '0',
  `show_phone` tinyint(1) NOT NULL DEFAULT '0',
  `photo` varchar(255) DEFAULT NULL,
  `gender` tinyint(1) UNSIGNED NOT NULL,
  `age` tinyint(2) UNSIGNED NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `level` tinyint(2) UNSIGNED NOT NULL,
  `dominant_hand` tinyint(1) UNSIGNED NOT NULL,
  `play_times` tinyint(2) UNSIGNED NOT NULL,
  `play_years` tinyint(2) UNSIGNED NOT NULL,
  `gear_1` varchar(100) DEFAULT NULL,
  `main_skill` tinyint(1) UNSIGNED NOT NULL,
  `pass` varchar(255) NOT NULL,
  `last_login` datetime NOT NULL,
  `time_edit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users_matches`
--

CREATE TABLE `users_matches` (
  `user_id` varchar(16) NOT NULL,
  `match_id` mediumint(8) UNSIGNED NOT NULL,
  `team` tinyint(1) NOT NULL COMMENT '1 - team 1, 2 - team 2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `match_dtime` (`match_dtime`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`,`ip_address`),
  ADD KEY `sessions_timestamp` (`timestamp`) USING BTREE;

--
-- Indexes for table `tournaments`
--
ALTER TABLE `tournaments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `start_date` (`start_date`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `matches`
--
ALTER TABLE `matches`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tournaments`
--
ALTER TABLE `tournaments`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;