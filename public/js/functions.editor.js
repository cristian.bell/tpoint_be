editor = {};
editor.selectedId = '';
editor.fonts = [];
editor.colors = ["000000", '7D87BC', 'ADB4DC', 'A4C7D5', '709EB0', "FFD09E", "FFEFDF", "FFE39E", "FFEDC1"];
editor.tpl_name = "";editor.tpl_name_tmp = editor.tpl_name;
var $selectedItem;
var texts = [];

/**
 * called when an item is selected - mouse click
 * @param ev
 */
function selectItem(ev){
    editor.selectedId = ev.target.id;
    $selectedItem = $("#"+editor.selectedId);
    $(".text_size").slider( "option", "disabled", false);
    $(".font_name").slider( "option", "disabled", false);
    $(".text_color").slider( "option", "disabled", false);
}

/**
 * checks if there is any editor item (element) selected.
 * @returns {boolean}
 */
function isElementSelected(){
    if (typeof (editor.selectedId) === "string" && editor.selectedId != "")
        return true;
    else
        return false;
}

/**
 * moves an item from the canvas to the menu (delete)
 * @param id
 * @param noDelete
 */
function moveToMenu(id, noDelete){
    if (arguments.length>0)
        editor.selectedId = id;
    $item = $("#"+editor.selectedId);
    var $newItem = $("<div id='" + editor.selectedId + "' />");
    $newItem.addClass( $item.attr('class') );
    $newItem.html($item.html());
    $newItem.appendTo($menu);
    $newItem.on("click", moveToCanvas);
    $item.remove();
    if (arguments.length < 2)
        getScript({item:editor.selectedId}, 'item/', '', '', 'DELETE');
    editor.selectedId = "";
    $selectedItem = null;
}

/**
 * moves an item from the menu to the canvas
 * @param ev
 * @param id
 * @param attributes
 * @returns {boolean}
 */
function moveToCanvas(ev, id, attributes){
    /*ev.preventDefault();
     var crtId = ev.dataTransfer.getData("text");*/
    var writeToDb = true;
    if (arguments.length > 1)
        var crtId = id;
    else
        var crtId = ev.target.id;

    if (crtId == "")
        return false;
    editor.selectedId = "";
    $selectedItem = null;
    $item = $("#"+crtId);

    $newItem = $item.clone(true).off();
    $newItem.on("mouseenter", function () {
        $(this).css("cursor", "pointer");
        outputInfo(texts["ED_CA_ITEM_OVER"]+"<br/>"+texts["ED_CA_ITEM_MOVE"]);
    }).on("mouseleave", function () {
        outputInfo('');
    }).on("click", function (event) {
        $(this).parent().children().removeClass("itemClick");
        $(this).addClass("itemClick");
        selectItem(event);
    });
    $item.remove();
    $newItem.draggable({
        start: function(event, ui){},
        drag: function(event, ui){},
        stop: function(event, ui){
            var fromTop = $(this).offset().top-$(this).parent().offset().top-1;
            var fromLeft = $(this).offset().left-$(this).parent().offset().left-1;
            var LTRBString = "";
            var B = parseInt($(this).parent().offset().top+$(this).parent().innerHeight())-5;
            var T = parseInt($(this).parent().offset().top)+5;
            var L = parseInt($(this).parent().offset().left)+5;
            var R = parseInt($(this).parent().offset().left+$(this).parent().innerWidth())-5;
            if (($(this).offset().top+$(this).outerHeight()) > B)
                LTRBString += "B";
            if (($(this).offset().top) < T)
                LTRBString += "T";
            if (($(this).offset().left) < L)
                LTRBString += "L";
            if (($(this).offset().left+$(this).outerWidth()) > R)
                LTRBString += "R";

            getScript(
                {item:crtId, align_parent:LTRBString, x:Math.round(ui.position.left), y:Math.round(ui.position.top),
                    x_rel: Math.round(fromLeft), y_rel: Math.round(fromTop)}, 'item/', '', '', 'PUT');
        },
        snap: true, snapTolerance: 1, addClasses: false, containment: "parent", cursor: "move"/*, scroll: false*/
    });
    $newItem.appendTo($canvas);
    /*if attributes are set, apply them: */
    if (arguments.length > 2 && typeof attributes == "object") {
        $newItem.css({"top":attributes.y+"px", "left":attributes.x+"px"});
        if (attributes.color.length > 0)
            $("#"+crtId).css("color", "#"+attributes.color);
        if (attributes.font_name.length > 0)
            $("#"+crtId).css("font-family", ""+attributes.font_name);
        if (parseInt(attributes.font_size) > 0 && parseInt(attributes.font_size) != 100)
            $("#"+crtId).css("font-size", ""+attributes.font_size+"%");
        if (parseInt(attributes.bold) > 0)
            $("#"+crtId).css("font-weight", 700);
        if (parseInt(attributes.italic) > 0)
            $("#"+crtId).css("font-style", "italic");
        writeToDb = false;
    }
    /*reset sliders*/
    $( ".text_size" ).slider( "value", 100 );
    $( ".font_name" ).slider( "value", 2 );
    $( ".text_color" ).slider( "value", 0 );
    if (writeToDb)
        getScript({item:crtId}, 'item/');
}

/**
 * sets the font size of the selected item
 * @param size
 */
function setItemSize(size){
    if (isElementSelected()){
        $("#"+editor.selectedId).css("font-size", size+"%");
        getScript({item:editor.selectedId, font_size:size}, 'item/', '', '', 'PUT');
    }
}

/**
 * sets the font family of the selected item
 * @param font
 */
function setItemFont(font){
    if (isElementSelected()){
        $("#"+editor.selectedId).css("font-family", font+"");
        getScript({item:editor.selectedId, font_name:font}, 'item/', '', '', 'PUT');
    }
}

/**
 * sets the font color of the selected item
 * @param color
 */
function setItemColor(color){
    if (isElementSelected()){
        $("#"+editor.selectedId).css("color", "#"+color+" ");
        getScript({item:editor.selectedId, color:color}, 'item/', '', '', 'PUT');
    }
}

/**
 * sets the font format of the selected item
 * @param ev
 */
function setItemFormat(ev){
    if (isElementSelected()){
        var valToSet = "";var toDb = 0;
        if (typeof ev.target.className == "string" && ev.target.className != ""){
            if (ev.target.className == "bold"){
                if ($selectedItem.css("font-weight") == "700"){
                    valToSet = "";
                    toDb = 0;
                } else {
                    valToSet = ev.target.className;
                    toDb = 1;
                }
                $selectedItem.css("font-weight", valToSet);
                getScript({item:editor.selectedId, bold:toDb}, 'item/', '', '', 'PUT');
            }else if (ev.target.className == "italic"){
                if ($selectedItem.css("font-style") == "italic") {
                    valToSet = "";
                    toDb = 0;
                } else {
                    valToSet = ev.target.className;
                    toDb = 1;
                }
                $selectedItem.css("font-style", valToSet);
                getScript({item:editor.selectedId, italic:toDb}, 'item/', '', '', 'PUT');
            }
        }
    }
}

/**
 * handles the calls from the sliderItemColor changes
 * @param event
 * @param ui
 */
function sliderItemColor(event, ui){
    if (isElementSelected()){
        var textColor = editor.colors[ui.value];
        $( "#text_color" ).html( "#" + textColor +"").css("background-color", "#"+textColor);
        setItemColor(textColor);
    }
}

/**
 * * handles the calls from the sliderItemFont changes
 * @param event
 * @param ui
 */
function sliderItemFont(event, ui){
    if (isElementSelected()) {
        var fontName = editor.fonts[ui.value];
        $("#font_name").html("" + fontName + "");
        setItemFont(fontName);
    }
}

/**
 * handles the calls from the sliderItemSize changes
 * @param event
 * @param ui
 */
function sliderItemSize(event, ui){
    if (isElementSelected()){
        $( "#text_size" ).html( "" + ui.value +"%");
        setItemSize(ui.value);
    }
}

/**
 * saves the template title.
 * If no template is already selected - no template id in the session, a new template will be created.
 */
function saveTemplateName(){
    if (editor.tpl_name != "" && editor.tpl_name != editor.tpl_name_tmp) {
        var body = {name:editor.tpl_name};
        var method = 'POST';
        //name change
        if (!dialogActive) {
            method = 'PUT';
        }
        $.ajax({
            url: "save/",
            data: JSON.stringify(body),
            method: method
        }).done(function (data) {
            var response = JSON.parse(data);
            if (typeof response.success == "boolean" && response.success) {
                editor.tpl_name_tmp = editor.tpl_name;
                nameTmp = editor.tpl_name;
                prepareEditorElements();
                /*if new template - add it to the select*/
                if (typeof response.new_id == "number" && response.new_id > 0) {
                    $('#all_templates').append($('<option>', {
                        value: response.new_id,
                        text: editor.tpl_name
                    })).val(response.new_id);
                    cleanCanvas();
                }
            }
        });
    }
}

function applyBackground(src){
    $canvas.css("background-image", "url('"+src+"')");
    src = src.replace(/http:\/\/[^\/]+/g, '');
    src = src.split("/");
    getScript({bg:src[src.length-1]}, 'save/', '', '', 'PUT');
}

/**
 * draws the canvas - called once a template is selected from the templates menu.
 * @param ev
 * @param id
 * @param name
 * @returns {boolean}
 */
function drawCanvas(ev, id, name){
    if (arguments.length == 1)
        id = ev.target.value;
    if (arguments.length < 3)
        name = $("#all_templates option:selected").text();

    if (id === 0)
        return false;

    $.get(window.location.href + "/getTemplateItems/" + id + "" , function (data) {
        cleanCanvas();
        if (data.length > 2){
            var response = JSON.parse(data);
            for(var i in response){
                if (typeof response[i].item == "string")
                    moveToCanvas({}, response[i].item, response[i]);
                else
                    if (typeof response[i].bg == "string" && response[i].bg != "")
                        $canvas.css("background-image", "url('"+response[i].bg+"')");
            }
        }
        editor.tpl_name = name;
        editor.tpl_name_tmp = editor.tpl_name;
        prepareEditorElements();
    });
}

/**
 * prepares the editor menu, canvas and template name for edit
 */
function prepareEditorElements(){
    $(".canvas,.menu").show();
    $(".name_save").hide();
    $(".name > input").val(editor.tpl_name);

    if (dialogActive){
        dialogTemplateHide($("#tpl_title"));
        dialogActive = !dialogActive;
    }
}

/**
 * clean the canvas and move all items back to the menu.
 */
function cleanCanvas(){
    $canvas.children("div").each(function () {
        moveToMenu($(this).attr("id"), false);
    });
    $canvas.css("background-image", "none");
}

var nameTmp = "";var dialogActive = false;
function dialogNewTemplate(){
    var crtTorturedSoul = $("#tpl_title");
    if (!dialogActive) {
        $(".mask").show();
        nameTmp = crtTorturedSoul.val();
        crtTorturedSoul.val("");
        crtTorturedSoul.attr("placeholder", "enter a template name please");
        $(".name").css({"position": "absolute", "top":"auto"}).animate({"left": "35%", "top": "+=20%"});
        $(".name").addClass('name_dialog');
        $(".name_save").show();
        $(".name_cancel").show();
        //z-index: -1;
    }else{
        dialogTemplateHide(crtTorturedSoul);
    }
    dialogActive = !dialogActive;
}

function dialogTemplateHide(torturedSoul){
    torturedSoul.val(nameTmp);
    $(".name").animate({"left": "0", "top": "-=20%"}, "slow", "", function () {
        $(this).css({"position": "relative"});
    });
    $(".name_save").hide();
    $(".name_cancel").hide();
    $(".name").removeClass('name_dialog');
    nameTmp = "";
    $(".mask").fadeOut(800);
}

function newTemplate(){
    //<?=base_url()?>Editor/newTemplate
}

/**
 * sends a message to the user. Requires a div with the id 'message' to work.
 * Levels are css classes, and for the time being just 4:
 *  Success (green border),
 *  Warn (orange border),
 *  Fail (orange border, light red background) and
 *  Advice (dark green)
 * @param message
 * @param autoClose [optional]
 * @param fadeOutDelay [optional]
 * @param level [optional]
 */
function sendMessage(message, autoClose, fadeOutDelay, level) {
    $('#message>.insideBox').html(message);
    $('#message').fadeIn();

    if (arguments.length > 0) {
        if (autoClose) {
            var delay = 1200;
            if (arguments.length > 2 && !isNaN(fadeOutDelay))
                delay = fadeOutDelay;
            $('#message').removeClass().addClass('messageBox');
            $('#message').delay(delay).slideUp(250);
        }
        if (arguments.length > 3 && typeof level == 'string') {
            $('#message').removeClass().addClass('messageBox messageBox'+level);
        }
    }
}

/**
 * used for ajax calls, default request method is GET. If parameter func is defined it will also call the func
 * function after the request is complete.
 * @param data
 * @param url
 * @param func
 * @param callSendMessage
 * @param verb
 */
function getScript(data, url, func, callSendMessage, verb) {
    var method = 'GET';
    if (arguments.length < 2 || (arguments.length > 1 && url == ''))
        url = 'index.php';
    if (arguments.length < 4)
        callSendMessage = false;
    if (typeof (data) == 'string') {
        url += '?' + data;
        data = {};
    } else {
        method = 'POST';
    }
    if (arguments.length > 4)
        method = verb;
    $.ajax({
        url: url,
        data: JSON.stringify(data),
        method: method
    }).done(function (response) {
        if (arguments.length > 2 && typeof func != 'undefined' && func != ''){
            eval(func+"("+response+")");
        }
        if (callSendMessage == true) {
            var message = JSON.parse(response);
            if (typeof (message.message) == 'string') {
                var autoClose = false;
                if (typeof message.success == 'boolean' && message.success == true)
                    autoClose = true;
                sendMessage(message.message, autoClose);
            }
        }
        //return(response);
        return true;
    });
}

/**
 * switch between the tool tabs
 * @param ev
 */
function switchTabs(ev){
    if (ev.target.className == 'sw_bg'){
        $("#menu, .tools").hide();
        $(".backgrounds").show();
    }else{
        $("#menu, .tools").show();
        $(".backgrounds").hide();
    }
}

/**
 * handles various key presses - for example removes the selected Element while pressing DEL.
 */
function handleKeys(key) {
    if (key == 46){
        if (isElementSelected())
            moveToMenu();
    }
}

/**
 * outputs info messages to the user
 * @param info
 */
function outputInfo(info){
    $(".info").html(info);
}

