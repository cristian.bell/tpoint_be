<?php

class MY_Model extends CI_Model
{

	protected static $databases = array();
	protected static $userIDLen = 12;
	public static $cities = array('Berlin', 'Hong Kong', 'Macau');

	public function __construct()
	{
		parent::__construct();
	}

	protected function _loadDatabase($db, $fetchMode = PDO::FETCH_ASSOC)
	{
		if (!isset(self::$databases[$db]))
		{
			$pdo = $this->load->database($db, TRUE)->conn_id;
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, $fetchMode);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return self::$databases[$db] = $pdo;
		}
		return self::$databases[$db];
	}

    /**
     * transform the mysql date format - Y-m-d to user readable data format - d.m.Y
     * @param $dateIn
     * @param bool $time
     * @param bool $no_yr
     * @return string
     * @throws Exception
     */
	public function formatOutputDate($dateIn, $time = false, $no_yr = false){
		if (strtotime($dateIn) > 0) {
			$date = new DateTime($dateIn);
			//$date->createFromFormat('Y-m-d H:i:s', $dateIn);
			if (!$time)
				return $date->format('d.m.Y');
			else
			    if ($no_yr)
                    return $date->format('d.m');
			    else
				    return $date->format('d.m.Y H:i');
		} else
			return "";

	}

    public function formatOutputDateMonth($dateIn, $time = false, $no_yr = false){
        if (strtotime($dateIn) > 0) {
            $date = new DateTime($dateIn);
            //$date->createFromFormat('Y-m-d H:i:s', $dateIn);
            if (!$time)
                return $date->format('d M Y');
            else
                if ($no_yr)
                    return $date->format('d M');
                else
                    return $date->format('d M Y H:i');
        } else
            return "";

    }

    /**
     * transform the user readable date format - d/m/Y to Y-m-d date format
     * @param $dateIn
     * @param $time
     * @return string
     * @throws Exception
     */
	public function formatInputDate($dateIn = "", $time = false) {
		$date = new DateTime($dateIn);
		$formatIn = 'd.m.Y';
		$formatOut = 'Y-m-d';
		if ($time) {
			$formatIn = 'd.m.Y H:i';
			$formatOut = 'Y-m-d H:i:s';
		}
		//$date->createFromFormat($formatIn, $dateIn);
		return $date->format($formatOut);
	}

	public function nowToDatestamp($time = null) {
		if (empty($time))
			$time = time();
		return date('Y-m-d H:i:s', $time);
	}

}
/*
name:'one'
start_date:'Mit Nov 21 2018 18:40:54 GMT+0100 (Mitteleuropäische Normalzeit)'
end_date:'Fre Nov 30 2018 13:40:54 GMT+0100 (Mitteleuropäische Normalzeit)'
*/
