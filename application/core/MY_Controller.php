<?php

class MY_Controller extends CI_Controller
{
	protected $appKey = '123';//needs to be changed for each major api update to block old clients app versions
    protected $checkAPIKey = false;
    protected $allowedMethods = array('delete', 'get', 'post', 'put', 'patch', 'options');
	protected $fields = array();
    protected $dataIn = array();
    public $inKey = '';
    private $APIKey = 123;
    protected static $scoreUpdTime = 'P2D';
    protected $MAX_IMG_SIZE = 0;
    protected $PHOTO_FOLDER = 'public/photos/';
    protected $usrImgPathStore = '';
    protected $usrImgPathWeb = '';
    protected $mailPath = 'third_party/Mailin.php';
    protected $mailInfo = '';
    protected $fromMail = 'tennis@matchpoint.pro';
    protected $fromMailName = 'MatchPoint.pro';
    protected $sendMails = true;

	public function __construct()
	{
        /*header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");*/
        if($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
            die();
        }
        parent::__construct();

        $this->load->helper('url');
        $this->MAX_IMG_SIZE = (1024 * 1024 * 100.5);
        $this->usrImgPathStore = FCPATH . $this->PHOTO_FOLDER;
        $this->mailInfo = $this->config->item('INFO');

        if ($_SERVER['SERVER_PORT'] != 80)
            $this->usrImgPathWeb = substr(base_url(), 0, -1) . ':' . $_SERVER['SERVER_PORT'] . '/' . $this->PHOTO_FOLDER;
        else
            $this->usrImgPathWeb = base_url() . $this->PHOTO_FOLDER;

        //var_dump($_SERVER);
        //check if requested method is allowed
        if (!in_array($this->input->method(), $this->allowedMethods)) {
            $this->notAllowed();
            //@todo should we log this?
        } else {
            switch ($this->input->method()) {
                case 'get':
                    $this->dataIn = $this->input->get();
                    break;
				case 'options':
				    die();
                case 'post':
                case 'delete':
				case 'patch':
                case 'put':
					$in = json_decode($this->input->raw_input_stream);

                    if (!empty($in) && is_object($in)) {
						$this->dataIn = get_object_vars($in);
					}
                    else {
						parse_str(file_get_contents("php://input"),$in);
						$this->dataIn = $in;
					}

                //catch all key=value post data
                if (empty($in) && $this->input->method() == 'post')
                    $this->dataIn = $this->input->post();
                break;
                default:
                    break;
            }

            //$this->load->helper('security');
            //$this->dataIn = $this->security->xss_clean($this->dataIn);
			//var_dump($this->security->get_random_bytes(16));

            $apiKey = html_escape($this->input->get_request_header('x-api-key', true));
            if ($this->authenticate($apiKey) === true) {
				$this->dataIn = html_escape($this->dataIn);
			}
        }
	}

	//@todo here only api key
	protected function authenticate($key):bool {
	    if (!$this->checkAPIKey) {
	        return true;
        } else {
			if ($key == $this->APIKey)
				return true;
			else {
				$this->inKey = $key;
				$this->logCalls("==>\nauth fail:");
				$this->forbidden();
				return false;
			}
        }
	}

	protected function authenticateAdmin($key = null, $uid = 0) {
		//@todo implement this
		if (!$this->checkAPIKey) {
			return true;
		} else {
			$this->forbidden();
		}
	}

    public function isUserLogin($showInfo = false):bool {
        $this->load->library('session');
        if ($showInfo) {
            //var_dump($this->session->user);
            var_dump($this->session->session_id);
            var_dump($_SESSION);
        }
        if (!empty($this->session) && isset($this->session->userLoggedIn) && $this->session->userLoggedIn === true)
            return true;
        else
            return false;
    }

    public function getLoggedUserId():string {
        $this->load->library('session');
        if (!empty($this->session) && isset($this->session->userLoggedIn) && $this->session->userLoggedIn === true) {
            return $this->session->user['id'];
        } else {
            $this->clean_session();
            return '';
        }
    }


    protected function forbidden($message = "HTTP/1.1 403 Forbidden")
	{
		http_response_code(403);
        header($message);
		$this->my_die($message);
	}

	protected function badRequest($message = "HTTP/1.1 400 Bad Request")
	{
		http_response_code(400);
        header($message);
        $this->my_die($message);
	}

	protected function notFound($message = "HTTP/1.1 404 Not found")
	{
		http_response_code(404);
        header($message);
        $this->my_die($message);
	}

	protected function notAllowed($message = "HTTP/1.1 405 Method Not Allowed")
	{
		http_response_code(405);
        header($message);
        $this->my_die($message);
	}

	protected function alreadyExists($message = "HTTP/1.1 409 Conflict")
	{
		http_response_code(409);
        header($message);
        $this->my_die($message);
	}

	protected function imATeapot($message = "HTTP/1.1 418 I'm a teapot")
	{
		http_response_code(418);
        header($message);
        $this->my_die($message);
	}

    protected function fileTooLarge($message = "HTTP/1.1 413 Payload Too Large")
    {
        http_response_code(413);
        header($message);
        $this->my_die($message);
    }

    protected function sendMail($subject, $body, $toMail, $toName = '') {
	    if ($this->sendMails === false)
	        return false;

        $mailFile = APPPATH . $this->mailPath;
        if (empty($subject) || empty($body) || empty($toMail))
            return false;
        if (empty($toName))
            $toName = $toMail;

        if (file_exists($mailFile) && !empty($this->mailInfo)) {
            require_once($mailFile);
            $mailin = new Mailin("https://api.sendinblue.com/v2.0", $this->mailInfo);
            $data = array(
                "to" => array($toMail => $toName),
                "from" => array($this->fromMail, $this->fromMailName),
                "subject" => $subject,
                "html" => $body,
            );
            $sendMail = $mailin->send_email($data);
            if ($sendMail['code'] === 'success') {
                return true;
            }
        }
        return false;
    }


    public function clean_session():bool {
        $this->load->library('session');
        if (isset($this->session)) {
            $this->session->sess_destroy();
            session_write_close();
            return true;
        }
        return false;
    }

	public function JSONOut($data, bool $JSON_headers = true) {
	    if ($JSON_headers)
            header('Content-Type: application/json');
		$out['out'] = json_encode($data);
		$this->load->view('JSON', $out);
	}

	//controlling hiawatha cache: header("X-Hiawatha-Cache: 100"); and header("X-Hiawatha-Cache-Remove: /script.php");
	protected function setNoCache() {
		header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
		header("Pragma: no-cache"); //HTTP 1.0
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
	}

	private function my_die($message = ''){
	    die($message);

    }

    public function logCalls($message) {
    	$message .= " " . date('d-m-Y H:i:s', time()) . " " . $_SERVER['REQUEST_URI'] . ", verb: " . $this->input->method(true) . "\ndataIn: ".serialize($this->dataIn).", inKey: ".$this->inKey;
        file_put_contents('outcalls.txt', "".$message. "\n", FILE_APPEND);
    }
}
