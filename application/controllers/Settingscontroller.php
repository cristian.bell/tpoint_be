<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settingscontroller extends MY_Controller {
    public $templateId = 0;
    protected $fields = array();
    protected $checkAPIKey = false;
    private $data;
    private $languages = ['english', 'german']; //this should be put somewhere else, but a quick fix for the moment

	public function __construct() {
		parent::__construct();

		if (!$this->authenticate(0,1))
			$this->forbidden();
		if ($this->input->method(true) != 'GET')
			$this->badRequest();
		$lang = (!empty($this->uri->segment(3)) && in_array($this->uri->segment(3), $this->languages)) ? $this->uri->segment(3) : '';
		$this->lang->load('messages_lang', $lang);
	}

	public function lang() {
		$data['out'] = json_encode($this->lang->language);
		$this->load->view('JSON', $data);
	}


	public function mysettings() {
		//output user settings
	}

}
