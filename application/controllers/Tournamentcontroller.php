<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Tournament ctrl
 * {
 * id,
 * gender,
 * level,
 * elim_rules,
 * date_start,
 * date_end,
 * courts,
 * location? {lat, lng}
 * sets ?? {1, 3 -> 5 ... 7}
 * }
 */
class Tournamentcontroller extends MY_Controller {
    public $SITE_DATA;
    protected $fields = array('id', 'name', 'creator_user_id', 'winner_user_id', 'singles', 'gender', 'level', 'elim_rules', 'start_date', 'end_date', 'courts', 'g_lat', 'g_lon', 'sets', 'city', 'location');
    protected $fieldsEdit = array('name', 'winner_user_id', 'singles', 'gender', 'level', 'elim_rules', 'courts', 'g_lat', 'g_lon', 'sets', 'city', 'location');
	protected $fields_user = array('user_id1', 'user_id2');
    protected $checkAPIKey = false;

    public function __construct() {
		parent::__construct();

        $this->lang->load('texts_lang');
		$this->lang->load('messages_lang');
        $this->load->model('Tournament');
        $this->Tournament->usrImgPathWeb = $this->usrImgPathWeb;
	}

	public function index() {
		redirect('/Welcome/index');
    }

    /**
     * add/edit match info
	 * only a logged in user can add/update a match
     */
    public function save() {
        $this->load->library('session');
        if (!$this->isUserLogin())
            $this->forbidden();
        try
		{
			$out = array();
			$time_edit = time();
			$success = true;

			//if POST add
			if ($this->input->method(true) == 'POST') {
                $objData = array_intersect_key($this->dataIn, array_flip($this->fields));
                if (!isset($objData['start_date']) || !isset($objData['end_date']) || !isset($objData['location'])
                    || !isset($objData['level']))
                    $this->badRequest();

                $startDate = $objData['start_date'];
                try
                {
                    $startDate = new DateTime(substr($startDate, 0, strpos($startDate, '(')));
                }
                catch (Exception $e) {
                    $this->logCalls("ERR: Tournament/save(): date field issue: ".__FILE__. ": ".__LINE__. 'e: '.$e);
                    $this->badRequest();
                    //$startDate = new DateTime();
                }
                $objData['start_date'] = $startDate->format('Y-m-d');

                try
                {
                    $endDate = $objData['end_date'];
                    $endDate = new DateTime(substr($endDate, 0, strpos($endDate, '(')));
                }
                catch (Exception $e) {
                    $this->logCalls("ERR: Tournament/save(): date field issue: ".__FILE__. ": ".__LINE__. 'e: '.$e);
                    $this->badRequest();
                    //$endDate = new DateTime();
                }
                $objData['end_date'] = $endDate->format('Y-m-d');
                $minStartDate = new DateTime('+ 7 day');
                $minEndDate = new DateTime('+ 14 day');
                if ($startDate < $minStartDate || $endDate < $minEndDate) {
                    $this->badRequest();
                }
                $objData['creator_user_id'] = $this->session->user['id'];
				$objID = $this->Tournament->add($objData);
				if ((int)$objID > 0) {
                    $this->load->model('Notification');
                    $this->Notification->addSystem('tournament_new', '', $objID);
                }
			}
			/*
			 * else if PATCH update/edit
			*/
			elseif ($this->input->method(true) == 'PATCH') {
			    if ($this->session->userLoggedIn !== true)
			        $this->forbidden();
				$objID = (int)$this->input->get_request_header('id', true);
				if (empty($objID)) {
					$this->notFound();
				}
				if ($this->Tournament->isCreator($this->session->user['id'], $objID)){
					$this->forbidden();
				}

                $objData = array_intersect_key($this->dataIn, array_flip($this->fieldsEdit));

				$this->Tournament->edit($objData, $objID);

				//users:
				if (isset($userData) && is_array($userData) && !empty($userData['user_id1']) && !empty($userData['user_id2'])) {
					$this->Match->deleteMatchUsers($objID);
					$success *= $this->Match->addMatchUsers($userData, $objID);
				}

			}
			else
				redirect('/Welcome/index');
			if ($success === false) {
				$out['success'] = false;
				$out['message'] = $this->lang->line('TXT_SAVE_FAIL');
				$out['message_code'] = 'TXT_SAVE_FAIL';
				$this->logCalls("ERR: ERR: Matchcontroller/save(): add() or addMatchUsers() failed");
			}
			else {
				$out['success'] = true;
				$out['message'] = $this->lang->line('TXT_SAVE_OK');
				$out['message_code'] = 'TXT_SAVE_OK';
				$out['id'] = $objID;
				$out['time_edit'] = $time_edit;
			}
			$this->JSONOut($out);
		}
		catch(Exception $err) {
			if(ENVIRONMENT == 'development')
				var_dump($err->getMessage());
			else {
				log_message("error",$err->getMessage());
				show_error($err->getMessage());
				return false;
			}
		}
    }

    public function deactivate($objID) {
        if (!$this->isUserLogin())
            $this->forbidden();
        $objID = (int)$objID;
        if (empty($objID)) {
            $this->notFound();
        }
        if ($this->Tournament->isCreator($this->session->user['id'], $objID)){
            $this->forbidden();
        }
        $this->Tournament->deactivate($objID);

    }

    public function get($id) {
		if ($this->input->method(true) == 'GET') {
			$data = $this->Tournament->getByID($id);
			$this->JSONOut($data);
		} else {
			$this->badRequest();
		}
	}

	public function listAll() {
		$this->JSONOut($this->Tournament->getAll());
	}

	public function listAllByUser($uid) {
		$this->JSONOut($this->Tournament->getAllByUser($uid));
	}

	public function listAllFuture() {
        $this->isUserLogin();
		$this->JSONOut($this->Tournament->getAll(true));
	}

	public function listAllFutureByUser($uid) {
		$this->JSONOut($this->Tournament->getAllByUser($uid, true));
	}

    public function join($id, $leave = false) {
        $this->load->library('session');
        if (!$this->isUserLogin())
            $this->forbidden();
        if ($this->input->method(true) == 'POST') {
            $data = array('user_id' => $this->session->user['id'], 'tournament_id' => $id);
            if (!$leave)
                $out = array('success' => $this->Tournament->join($data));
            else
                $out = array('success' => $this->Tournament->leave($data));
            $this->JSONOut($out);
        } else {
            $this->badRequest();
        }
    }

    public function leave($id) {
        $this->join($id,true);
    }

    public function start($objID) {
        $userID = $this->getLoggedUserId();
        /*if (empty($userID) || strlen(($userID)) < $this->config->item('MIN_USER_ID_LEN'))
            $this->forbidden();

        if ($this->Tournament->isCreator($userID, $objID)){
            $this->forbidden();
        }*/

        $objID = (int)$objID;
        $tournamentData = $this->Tournament->getByID($objID);
        if (empty($objID) || empty($tournamentData)) {
            $this->notFound();
        }

        if (count($tournamentData['users']) < $this->config->item('MIN_USER_TO_START_TOUR') && count($tournamentData['users']) % 2 == 0)
            $this->badRequest();

        shuffle($tournamentData['users']);

        $dataMatch = array();
        $this->load->model('Match');
        foreach (array_chunk($tournamentData['users'], 2) as $pairs) {
            //addMatch
            // = array('tournament_id' => $objID);
            //using the id, add the match_users:
        }

        //$pairs[0]['id'], $pairs[1]['id']
        var_dump($dataMatch);

        //create first match batch
        //save to the db
        //send notifications for each
    }

}
