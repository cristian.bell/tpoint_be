<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Usercontroller
 */
class Matchcontroller extends MY_Controller {
    public $SITE_DATA;
    protected $topFile = 'Top';
    protected $fields = array('id', 'location', 'match_dtime', 'winner', 'score', 'score_dtime', 'score_conf', 'score_added_by');
	protected $fields_user = array('user_id1', 'user_id2');
    protected $checkAPIKey = false;

    public function __construct() {
		parent::__construct();

        $this->lang->load('texts_lang');
		$this->lang->load('messages_lang');
        $this->load->model('Match');
	}

	public function index() {
		redirect('/Welcome/index');
    }

    /**
     * add/edit match info
	 * only a logged in user can add/update a match
     */
    public function save() {
		$this->load->library('session');
		try
		{
			$out = array();
			$time_edit = time();
			$success = true;
			$userData = array_intersect_key($this->dataIn, array_flip($this->fields_user));
			$matchData = array_intersect_key($this->dataIn, array_flip($this->fields));

			//if POST add
			if ($this->input->method(true) == 'POST') {
				if ($this->session->userLoggedIn !== true ||
					((strpos($userData['user_id1'], $this->session->user['id']) === false) && (strpos($userData['user_id2'], $this->session->user['id']) === false))){
					$this->forbidden();
				}
				//$this->authenticate();
				if (empty($matchData['location']) || empty($matchData['match_dtime']) || empty($userData['user_id1'])
					|| empty($userData['user_id2'])) {
					$this->logCalls("ERR: Matchcontroller/save(): a required field was empty at: ");
					$this->badRequest();
				}
				//todo: think about if checking if match is already saved ?!
				/*
				if (false && $this->User->getUserByMail($userData['email'], '') != null) {
					$this->logCalls("ERR: User already exists: e: " . $userData['email'] . ', p: ' . $userData['pass'] . ', ip: ' . $_SERVER['REMOTE_ADDR']);
					$this->alreadyExists();
					//correct:
					//send e-mail with login attempt
					//$this->badRequest();
				}*/

				$matchID = $this->Match->add($matchData);
				if ($matchID != false && !empty($matchID))
					$success *= $this->Match->addMatchUsers($userData, $matchID);
			}
			/*
			 * else if PATCH update/edit
			 * only one of the match players can update a match.
			 * only future matches info can be updated, else only score can be added and confirmed
			 *
			*/

			elseif ($this->input->method(true) == 'PATCH') {
				$matchID = (int)$this->input->get_request_header('mid', true);
				if (empty($matchID)) {
					$this->notFound();
				}
				$userDataInDb = $this->Match->getMatchUsers($matchID);
				if ($this->session->userLoggedIn !== true || !in_array($this->session->user['id'], $userDataInDb)){
					$this->forbidden();
				}
				$matchDataInDb = $this->Match->getByID($matchID);
				/* score
				 * score can only be updated  hours after being set
				 * score can not be updated once confirmed
				 */
				if (strtotime($matchDataInDb['dtime']) < time()) {
					unset($matchData['location']);
					unset($matchData['match_dtime']);
					unset($userData);
				}

				if (!empty($matchDataInDb['score']) && !empty($matchDataInDb['score_conf'])) {
					$this->notAllowed();
				}
				if (!empty($matchData['score']) && strtotime($matchDataInDb['score_added']) > 0) {
					$now = new DateTime();
					$scoreEditEnd = new DateTime($matchDataInDb['score_added']);
					$scoreEditEnd->add(new DateInterval(parent::$scoreUpdTime));
					if ($now > $scoreEditEnd) {
						$this->notAllowed();
					}
				}
				if (!empty($matchDataInDb['score_added']) && strtotime($matchDataInDb['score_added']) > 0)
					$matchData['score_added'] = $matchDataInDb['score_added'];
				$this->Match->edit($matchData, $matchID);

				//users:
				if (isset($userData) && is_array($userData) && !empty($userData['user_id1']) && !empty($userData['user_id2'])) {
					$this->Match->deleteMatchUsers($matchID);
					$success *= $this->Match->addMatchUsers($userData, $matchID);
				}

			}
			else
				redirect('/Welcome/index');
			if ($success === false) {
				$out['success'] = false;
				$out['message'] = $this->lang->line('TXT_SAVE_FAIL');
				$out['message_code'] = 'TXT_SAVE_FAIL';
				$this->logCalls("ERR: ERR: Matchcontroller/save(): add() or addMatchUsers() failed");
			}
			else {
				$out['success'] = true;
				$out['message'] = $this->lang->line('TXT_SAVE_OK');
				$out['message_code'] = 'TXT_SAVE_OK';
				$out['id'] = $matchID;
				$out['time_edit'] = $time_edit;
			}
			$this->JSONOut($out);
		}
		catch(Exception $err) {
			if(ENVIRONMENT == 'development')
				var_dump($err->getMessage());
			else {
				log_message("error",$err->getMessage());
				show_error($err->getMessage());
				return false;
			}
		}
    }

	/**
	 *
	 */
    public function get($id) {
		if ($this->input->method(true) == 'GET') {
			$data = $this->Match->getByID($id);
			$this->JSONOut($data);
		} else {
			$this->badRequest();
		}
	}

	public function confirm($id) {
        $uid = $this->getLoggedUserId();
        if (!empty($uid)) {
            $matchData = $this->Match->getByID($id);

            if (isset($matchData['score_added_by']) && $matchData['score_added_by'] !== $uid
                && $matchData['score_conf'] !== '1') {

                $data = array(
                    'score_conf_by' => $uid,
                    'score_conf' => 1);
                $this->Match->edit($data, $id);
                $this->Match->computeWinner($id);
            } else
                $this->badRequest();
        }
    }

    public function dispute($id) {
        $uid = $this->getLoggedUserId();
        if (!empty($uid)) {
            if ($this->Match->isDisputable($id, $uid)) {
                $username = $this->session->user['firstname'] . ' ' . $this->session->user['lastname'];
                $body = 'Hi, <br/> The entered score was disputed by ' . $username . ' with the e-mail: ' . $this->session->user['email'];
                $body .= '.<br/>An admin will contact you shortly to resolve this matter.';
                $this->sendMail('Score dispute', $body, $this->fromMail);
                $this->sendMail('Score dispute', $body, $this->session->user['email']);
                $this->JSONOut(array('success' => true));
            } else {
                $this->badRequest();
            }
        }
    }

	public function listAll() {
		$this->JSONOut($this->Match->getAll());
 	}

	public function listAllByUser($uid = null) {
        if (empty($uid))
            $uid = $this->getLoggedUserId();
		$this->JSONOut($this->Match->getAllByUser($uid));
	}

	public function listAllFuture() {
		$this->JSONOut($this->Match->getAll(true));
	}

	public function listAllFutureByUser($uid = null) {
        if (empty($uid))
            $uid = $this->getLoggedUserId();
		$this->JSONOut($this->Match->getAllByUser($uid, true));
	}

}
