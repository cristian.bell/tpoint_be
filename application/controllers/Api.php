<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller
{

	const MAX_DISTANCE = 25;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Apimodel');
		$this->load->model('User');
	}

	public function saveUser($title, $firstName, $lastName, $jobTitle, $phone1, $email, $address1 = '', $address2 = '', $postalCode = '', $city = '', $country = '', $company = '', $phone2 = '', $fax = '', $website = '', $slogan = '', $pin = '', $userID = '')
	{
		if ($pin != '' && $userID != '')
		{
			$this->authenticate($userID, $pin); //dies if not auth
			if ($address1 == '' && $address2 == '' && $postalCode == '' && $city == '' && $country == '' && $company == '' && $phone2 == '' && $fax == '' && $website == '' && $slogan == '')
			{
				$this->Apimodel->editUserShort($userID, $title, $firstName, $lastName, $jobTitle, $phone1, $email);
			}
			else
			{
				$this->Apimodel->editUser($userID, $title, $firstName, $lastName, $jobTitle, $phone1, $email, $address1, $address2, $postalCode, $city, $country, $company, $phone2, $fax, $website, $slogan);
			}
			die(json_encode(['success' => TRUE, 'userID' => $userID])); //@TODO soll auch hier key + timeEdit mitgeschickt werden?
		}
		$pin = $this->User->generatePin();
		$result = $this->Apimodel->createUser($title, $firstName, $lastName, $jobTitle, $phone1, $email, $address1, $address2, $postalCode, $city, $country, $company, $phone2, $fax, $website, $slogan, $pin);
		if ($result === NULL)
		{
			die(json_encode(['success' => FALSE, 'message' => '@TODO', 'messageCode' => '@TODO']));
		}
		die(json_encode(['success' => FALSE, 'userID' => $result['userID'], 'timeEdit' => $result['timeEdit'], 'key' => $result['key']]));
	}

	public function updateLocation($userID, $pin, $latitude, $longitude)
	{
		$this->authenticate($userID, $pin); //dies if not auth
		$this->Apimodel->updateLocation($userID, $latitude, $longitude);
		die(json_encode(['success' => TRUE]));
	}

	public function getNearbyUsers($userID, $pin, $maxDistance = self::MAX_DISTANCE)
	{
		$user = $this->authenticate($userID, $pin); //dies if not auth
		$maxDistance = (int) $maxDistance;
		if ($maxDistance > self::MAX_DISTANCE || $maxDistance < 1)
		{
			$maxDistance = self::MAX_DISTANCE;
		}
		$otherUsers = $this->Apimodel->getNearbyUsers($userID, $maxDistance, $user['latitude'], $user['longitude']);
		die(json_encode(['maxDistance' => $maxDistance, 'nearbyUsers' => $otherUsers]));
	}

}
