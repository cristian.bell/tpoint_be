<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Usercontroller
 */
class Usercontroller extends MY_Controller {
    public $SITE_DATA;
    const PARAM_PLACEHOLDER = "#xyz";
    const MAX_DISTANCE = 200;//25;
	const MIN_PASS_LEN = 5;//25;
    protected $topFile = 'Top';
    protected $fields = array('firstname', 'lastname', 'email', 'phone', 'show_email', 'show_phone', 'photo', 'gender',
		'age', 'age', 'city', 'country', 'level', 'dominant_hand', 'backhand_style',
		'play_times', 'play_years', 'gear_1', 'main_skill', 'pass');
    protected $checkAPIKey = false;

    public function __construct() {
		parent::__construct();

        $this->lang->load('texts_lang');
		$this->lang->load('messages_lang');
        $this->load->model('User');
	}

	public function index() {
		redirect('/Welcome/index');
    }

    /**
     * add a new user form should be here
     */
    public function useradd() {
        $data['headerinfo']='';

        $this->load->view('Top', $data);
        $this->load->view('useradd', $data);
        $this->load->view('Bottom', $data);
    }

    public function save() {
		try
		{
			$out = array();
			$time_edit = time();
			$affected = 0;
			$success = false;
			$login = false;
			$userData = array_intersect_key($this->dataIn, array_flip($this->fields));

			//if POST add
			if ($this->input->method(true) == 'POST') {
				//$this->authenticate();
				//$this->logCalls("LOG: save()");
                $city = (int)$userData['city'];
                $level = (int)$userData['level'];

				if (empty($userData['firstname']) || empty($userData['email']) || (!isset($level) || $level < 0)
					|| (!isset($city) || $city < 0) /*|| empty($userData['country'])*/ || empty($userData['pass'])
					|| strlen($userData['pass']) < self::MIN_PASS_LEN) {
					$this->logCalls("ERR: a required field was empty or password was too short");
					$this->badRequest();
				}

				if ($this->User->getUserByMail($userData['email']) !== null) {
					$this->logCalls("ERR: User already exists: e: " . $userData['email'] . ', p: ' . $userData['pass'] . ', ip: ' . $_SERVER['REMOTE_ADDR']);
					$this->sendMail('Did you forget your password?',
                        'Hi! <br/> Someone tried to sign up with your e-mail on MatchPoint. If you forgot your password, you can reset it.', $userData['email']);
                    $this->badRequest();
				}

				$success = $this->User->addUser($userData);
				$uid = $success;
				$affected = 1;
				if (!empty($uid)) {
                    $this->sendMail('Welcome to MatchPoint',
                        'Hi there! <br/> Welcome to a MatchPoint, a community of amazing tennis players!', $userData['email']);
                }
				//login the user
                // todo check this
                /*
				$login = $this->login($userData);
				if ($login)
					return true;
                */
                $this->JSONOut(array('success' => true), !false);
			}
			//else if PATCH update/edit
			elseif ($this->input->method(true) == 'PATCH') {
				//do not allow pwd update here
				if (key_exists('pass', $userData))
					unset($userData['pass']);
				//check if user exists in the db based on the session login id.
                $uid = $this->getLoggedUserId();
                if (!empty($uid) && $this->User->getMail($uid) != null) {
                    $success = $this->User->editUser($userData, $uid);
                    $affected = $this->db->affected_rows();
                } else
                    $this->forbidden();
			}
			else {
				$this->badRequest();
			}

			if ($success === false) {
				$out['success'] = false;
				$out['message'] = $this->lang->line('TXT_SAVE_FAIL');
				$out['message_code'] = 'TXT_SAVE_FAIL';
                $this->logCalls("ERR: userSave failed");
			}
			else {
				$out['success'] = true;
				$out['message'] = $this->lang->line('TXT_SAVE_OK');
				$out['message_code'] = 'TXT_SAVE_OK';
				$out['uid'] = $uid;
                $this->logCalls("NOTICE: userSave success");
				if($affected > 0)
					$out['time_edit'] = $time_edit;
				else {
					$out['message'] = $this->lang->line('TXT_NO_SAVE');
					$out['message_code'] = 'TXT_NO_SAVE';
				}
			}
			$this->JSONOut($out);
		}
		catch(Exception $err) {
			if(ENVIRONMENT == 'development')
				var_dump($err->getMessage());
			else {
				log_message("error",$err->getMessage());
				show_error($err->getMessage());
				return false;
			}
		}
    }

    public function get($uid) {
		if ($this->input->method(true) == 'GET' && ($this->uri->segment(2) != 'login')) {
			$data = array_merge($this->User->getUserByID($uid, '', $this->usrImgPathWeb, $this->usrImgPathStore), $this->User->getUserMatches($uid), $this->User->getUserRank($uid));

			//if

			if (empty($data['firstname']))
				$data = array();
			$this->JSONOut($data);
		} else {
			$this->badRequest();
		}
	}

	/**
	 * add user info
	 * @param null $userArr
	 * @return bool
	 */
	public function login($userArr = null) {
		if ($this->input->method(true) !== 'POST')
			$this->imATeapot();

        $this->load->library('session');

		if (!empty($userArr) && is_array($userArr)) {
			foreach ($this->fields as $field) {
				if (!key_exists($field, $userArr))
					$userArr[$field] = null;
			}
			$this->session->userLoggedIn = true;
			$this->session->user = $userArr;
			$this->JSONOut($userArr);
			return true;
		}
		try
		{
			$userData = array_intersect_key($this->dataIn, array_flip($this->fields));

			//if POST login
			//$this->load->helper('email');
			if ($this->input->method(true) == 'POST' && filter_var($userData['email'], FILTER_VALIDATE_EMAIL)) {
				if (empty($userData['email']) || empty($userData['pass'])) {
					$this->logCalls("ERR: some field was empty");
					$this->badRequest();
				}

                //if there is a cookie and the user is logged in
                if ($this->isUserLogin()) {
                    if ($userData['email'] === $this->session->user['email'])
                        $this->JSONOut($this->session->user);
                }

				$user = $this->User->getUserByMailPass($userData['email'], $userData['pass'], $this->usrImgPathWeb, $this->usrImgPathStore);
				if ($user != null) {
					$this->session->userLoggedIn = true;
					$this->session->user = $user;
					//update last login
					$this->User->updateLastLogin();
				} else {
                    $this->clean_session();
                    $this->notFound();
                }
				$this->JSONOut($user, !false);
			}
			else
				$this->badRequest();
		}
		catch(Exception $err) {
			if(ENVIRONMENT == 'development')
				var_dump($err->getMessage());
			else {
				log_message("error",$err->getMessage());
				return false;
			}
		}
	}

	public function logout() {
        $success = false;
	    if ($this->input->method(true) === 'POST') {
            $success = $this->clean_session();
        }
		$this->JSONOut(array('success' => $success));
	}

	public function relogin() {}

	/**
	 *
	 */
	public function me() {
		if ($this->input->method(true) == 'GET' && $this->session->userLoggedIn == true) {
            $this->load->library('session');
			$this->JSONOut($this->session->user);
		} else
			$this->notAllowed();
	}

    public function photo() {
	    $userID = $this->getLoggedUserId();
	    if (empty($userID))
	        $this->forbidden();

	    $success = false;
        $filename = $userID . '_' . '.jpg';
	    if ($_SERVER['CONTENT_LENGTH'] > $this->MAX_IMG_SIZE)
	        $this->fileTooLarge();
	    $base64_delim = 'data:image/jpeg;base64,';

	    //var_dump($this->dataIn);
	    $key = key($this->dataIn);
        $start = strpos($key, 'Content-Disposition:');
        $key = trim(substr($key, 0, $start));

	    $image = array_reverse($this->dataIn);
        $image = array_pop($image);
        $start = strpos($image, $base64_delim);
        $end = strrpos($image, $key);
        //var_dump($image);
        if ($start !== false && $end !== false) {
            $start += strlen($base64_delim);
            $image = trim(substr($image, $start, $end - $start));
            $image = str_replace(' ', '+', $image);
            $file = fopen($this->usrImgPathStore . $filename, 'wb' );
            $success = fwrite($file, base64_decode($image));
            fclose($file);
            if ($success !== false) {
                $data = array('photo' => $filename);
                $success = $this->User->addPhoto($data, $userID);
            }
        }
        $this->JSONOut(array('success' => $success));
    }

	/**
	 * POST/PUT
	 */
    public function befriend() {
		try
		{
			$out = array();
			$success = false;
			$uid = isset($this->dataIn['uid']) ? (int)$this->dataIn['uid'] : 0;
			$uid2 = isset($this->dataIn['uid2']) ? (int)$this->dataIn['uid2'] : 0;
			if (empty($uid) || empty($uid2))
				$this->badRequest();
			$this->authenticate($uid);

			if ($this->User->getMail($uid) == null || $this->User->getMail($uid2) == null)
				$this->badRequest();

			//if POST update
			if ($this->input->method(true) == 'POST') {
				if ($this->User->checkFriends($uid, $uid2) != true)
					$this->badRequest();
				$success = $this->User->removeFriend($uid, $uid2);
			}
			//else if PUT add
			elseif ($this->input->method(true) == 'PUT') {
				if ($this->User->checkFriends($uid,$uid2) == true)
					$this->alreadyExists();
				$success = $this->User->addFriend($uid, $uid2);
			}
			else
				redirect('/Welcome/index');
			if ($success === false) {
				$out['success'] = false;
				$out['message'] = $this->lang->line('TXT_SAVE_FAIL');
				$out['message_code'] = 'TXT_SAVE_FAIL';
			}
			else {
				$out['success'] = true;
				$out['message'] = $this->lang->line('TXT_SAVE_OK');
				$out['message_code'] = 'TXT_SAVE_OK';
			}
			$data['out'] = json_encode($out);
			$this->load->view('JSON',$data);
		}
		catch(Exception $err) {
			if(ENVIRONMENT == 'development')
				var_dump($err->getMessage());
			else {
				log_message("error",$err->getMessage());
				show_error($err->getMessage());
				return false;
			}
		}
	}

	/**
	 * POST
	 * @return bool
	 */
	public function confirm_friend() {
		try
		{
			$out = array();
			$success = false;
			$uid = isset($this->dataIn['uid']) ? (int)$this->dataIn['uid'] : 0;
			$uid2 = isset($this->dataIn['uid2']) ? (int)$this->dataIn['uid2'] : 0;
			if (empty($uid) || empty($uid2))
				$this->badRequest();
			$this->authenticate($uid);

			if ($this->User->getMail($uid) == null || $this->User->getMail($uid2) == null)
				$this->badRequest();

			if ($this->input->method(true) == 'POST') {
				if ($this->User->checkFriends($uid, $uid2) != true)
					$this->badRequest();
				$success = $this->User->confirmFriend($uid, $uid2);
			}
			else
				redirect('/Welcome/index');
			if ($success === false) {
				$out['success'] = false;
				$out['message'] = $this->lang->line('TXT_SAVE_FAIL');
				$out['message_code'] = 'TXT_SAVE_FAIL';
			}
			else {
				$out['success'] = true;
				$out['message'] = $this->lang->line('TXT_SAVE_OK');
				$out['message_code'] = 'TXT_SAVE_OK';
			}
			$data['out'] = json_encode($out);
			$this->load->view('JSON',$data);
		}
		catch(Exception $err) {
			if(ENVIRONMENT == 'development')
				var_dump($err->getMessage());
			else {
				log_message("error",$err->getMessage());
				show_error($err->getMessage());
				return false;
			}
		}
	}

	/**
	 * POST
	 */
	public function location() {
		$out = array();
		if ($this->input->method(true) != 'POST')
			$this->badRequest();
		$uid = isset($this->dataIn['uid']) ? (int)$this->dataIn['uid'] : 0;

		$this->authenticate($uid);
		if ($this->User->getMail($uid) == null)
            $this->badRequest();

		$success = $this->User->updateLocation($uid, $this->dataIn['lat'], $this->dataIn['lng']);
		if ($success === false) {
			$out['success'] = false;
			$out['message'] = $this->lang->line('TXT_SAVE_FAIL');
			$out['message_code'] = 'TXT_SAVE_FAIL';
		}
		else {
			$out['success'] = true;
			$out['message'] = $this->lang->line('TXT_SAVE_OK');
			$out['message_code'] = 'TXT_SAVE_OK';
		}
        $data['out'] = json_encode($out);
        $this->load->view('JSON',$data);
	}

	/**
	 * GET
	 */
	public function nearby() {
		$this->setNoCache();
		if ($this->input->method(true) != 'GET')
			$this->badRequest();
		$uid = isset($this->dataIn['uid']) ? (int)$this->dataIn['uid'] : 0;
        if (empty($uid))
            $uid = (int)$this->uri->segment(3);
		$this->authenticate($uid);
        if ($this->User->getMail($uid) == null)
            $this->badRequest();
        $out = $this->User->getNearbyUsers($uid, self::MAX_DISTANCE);
        $data['out'] = json_encode($out);
        $this->load->view('JSON',$data);
	}

	/**
	 * GET
	 */
	public function requests() {
		$this->setNoCache();
		if ($this->input->method(true) != 'GET')
			$this->badRequest();
		$uid = (int)$this->uri->segment(3);

		if ($uid == 0 || $this->User->getMail($uid) == null)
			$this->badRequest();
		$out = $this->User->getRequests($uid, self::MAX_DISTANCE);
		$this->load->view('JSON', array('out' => json_encode($out)));
	}

	/**
	 * POST
	 */
	public function search() {
		if ($this->input->method(true) != 'POST')
			$this->badRequest();
		$uid = isset($this->dataIn['uid']) ? (int)$this->dataIn['uid'] : 0;
		$search_string = isset($this->dataIn['search_string']) ? (string)$this->dataIn['search_string'] : '';
		$out = $this->User->getOwnContacts($uid, $search_string);
		$this->load->view('JSON', array('out' => json_encode($out)));
	}

	public function test2() {
		$this->setNoCache();
		$out = array('m' => $this->input->method(true), 'one' => 1, 't' => date('d-m H:i:s', time()));
		$this->load->view('JSON', array('out' => json_encode($out)));
	}

    public function test() {
    }

}
