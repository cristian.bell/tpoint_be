<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('Usercontroller.php');
/**
 * Messagescontroller
 */
class Messagescontroller extends Usercontroller {
    protected $topFile = 'Top';
    protected $fields = array('uid', 'message', 'key');
    protected $checkAPIKey = false;

    public function __construct() {
		parent::__construct();

        $this->load->model('Messages');
	}
	
    /**
     * add
     */
    public function index() {
		try
		{
			$out = [];
			$success = false;
			$messageData = array_intersect_key($this->dataIn, array_flip($this->fields));

			if ($this->input->method(true) == 'PUT') {

                if (empty($messageData['message']) || empty($messageData['uid']) || $this->User->getUserByID($messageData['uid']) == null) {
                    $this->badRequest();
                }

				$this->authenticate();

				$messageData['id_user'] = $messageData['uid'];
                unset($messageData['uid']);
                unset($messageData['key']);
                $success = $this->Messages->addMessage($messageData);
			} elseif ($this->input->method(true) == 'GET') {
				$this->authenticateAdmin();
				$dataOut= $this->Messages->getAll();
			}
			else
				redirect('/Welcome/index');

			$out['success'] = $success;
			if (!empty($dataOut)) {
				$out = $dataOut;
			}
			elseif ($success === false) {
				$out['message'] = $this->lang->line('TXT_SAVE_FAIL');
				$out['message_code'] = 'TXT_SAVE_FAIL';
			}
			else {
				$out['message'] = $this->lang->line('TXT_SAVE_OK');
				$out['message_code'] = 'TXT_SAVE_OK';
			}
			$data['out'] = json_encode($out);
			$this->load->view('JSON',$data);
		}
		catch(Exception $err) {
			if(ENVIRONMENT == 'development')
				var_dump($err->getMessage());
			else {
				log_message("error",$err->getMessage());
				show_error($err->getMessage());
				return false;
			}
		}
    }

}
