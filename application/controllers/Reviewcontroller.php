<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 */
class Reviewcontroller extends MY_Controller {
    public $SITE_DATA;
    protected $fieldsEdit = array('character', 'technique', 'review_text', 'manner', 'punctuality', 'consistency',
        'power', 'strategy', 'score');
    protected $fields = array('match_id');
    protected $checkAPIKey = false;
    private $userId = '';

    public function __construct() {
        parent::__construct();

        $this->lang->load('texts_lang');
        $this->lang->load('messages_lang');
        $this->load->model('Review');

        $this->fields = array_merge($this->fields, $this->fieldsEdit);

        $this->userId = $this->getLoggedUserId();
        if (empty($this->userId))
            $this->forbidden();
    }

    public function index() {
        redirect('/Welcome/index');
    }

    public function get(string $userID, int $id) {
        $this->JSONOut($this->Review->get($userID, $id));
    }

    public function getAll($userID = null) {
        if (empty($userID))
            $userID = $this->userId;
        $this->JSONOut($this->Review->getAll($userID));
    }

    public function save() {
        $objData = array_intersect_key($this->dataIn, array_flip($this->fields));

        //if POST add
        if ($this->input->method(true) == 'POST') {
            $match_id = (int)$objData['match_id'];
            if (empty($match_id) || !isset($objData['manner']) || !isset($objData['punctuality'])
                || !isset($objData['power']) || !isset($objData['consistency']) || !isset($objData['strategy']))
                $this->badRequest();

            if (!empty($objData['score'])) {
                $this->load->model('Match');
                $matchData = array(
                    'score' => $objData['score'],
                    'score_added_by' => $this->userId,
                );
                unset($objData['score']);
                $this->Match->edit($matchData, $match_id);
            }

            $objData['user_id_writer'] = $this->userId;
            $this->JSONOut($this->Review->add($objData));

        } elseif ($this->input->method(true) == 'PATCH') {
            if (!isset($objData['manner']) || !isset($objData['punctuality'])
                || !isset($objData['power']) || !isset($objData['consistency']) || !isset($objData['strategy']))
                $this->badRequest();

        }
    }

}