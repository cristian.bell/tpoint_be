<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Editorcontroller
 */
class Editorcontroller extends MY_Controller {
    public $templateId = 0;
    protected $fields = array('name', 'bg', 'enabled', 'id_user', 'portrait', 'item', 'x', 'y', 'x_rel', 'y_rel',
        'align_parent', 'font_name', 'font_size', 'color', 'bold', 'italic');
    protected $checkAPIKey = false;

	public function __construct() {
		parent::__construct();

		$this->lang->load('texts_lang');
		$this->lang->load('messages_lang');

		$this->load->model('Editor');
        session_start();
	}

    /**
     * GET
     */
	public function index() {
        if (!$this->authenticate(0,1))
            $this->forbidden();
        if ($this->input->method(true) != 'GET')
            $this->badRequest();
        //session_destroy();
        $data['tpl_id'] = 0;
        $data['tpl'] = new stdClass();
        $data['tpl']->name = "";
        $data['templates'] = [];
        if (isset($_SESSION['tpl_id']) && (int)$_SESSION['tpl_id'] > 0) {
            $data['tpl_id'] = (int)$_SESSION['tpl_id'];
            $templateInfo = $this->Editor->getTemplateInfo($data['tpl_id']);
            if(isset($templateInfo->name)) {
                $data['tpl'] = $templateInfo;
                $data['tpl']->name = urldecode($data['tpl']->name);
            }
        } else {
            $data['templates'][0] = '- please choose -';
        }

		$data['editorMode'] = true;
		$data['headerinfo'] = "";
        $data['templates'] = $data['templates'] + $this->Editor->getAllTemplates(true);
		$data['fonts'] = $this->Editor->getEnabledFonts(true);

		$this->load->view('Top',$data);
		$this->load->view('editor', $data);
		$this->load->view('BottomEditor',$data);
	}

    /**
     * resets the current template id session var
     */
    public function reset() {
        $_SESSION['tpl_id'] = 0;
    }

    /**
     * POST/PUT
     */
    public function save() {
        if (!$this->authenticate(0,1))
            $this->forbidden();
        $data = array_intersect_key($this->dataIn, array_flip($this->fields));
        $data['time_edit'] = time();
        $out = [];
        //if POST add
        if ($this->input->method(true) == 'POST') {
            $success = false;
            //create new templates only for requests with name
            if (key_exists("name", $data)) {
                $data['enabled'] = 1;
                $success = $this->Editor->addTemplate($data);
                if ($success != false) {
                    $this->reset();
                    $out['new_id'] = (int)$success;
                    $_SESSION['tpl_id'] = $out['new_id'];

                    $success = true;
                }
            }
        } else if ($this->input->method(true) == 'PUT') {
            $success = $this->Editor->editTemplate($data, $_SESSION['tpl_id']);
        } else
            $this->badRequest();

        $out['success'] = $success;
        if(!$success) {
            $out['message'] = $this->lang->line('TXT_SAVE_NOK');
            $out['message_code'] = 'TXT_SAVE_FAIL';
        }else{
            $out['success'] = true;
            $out['message'] = $this->lang->line('TXT_SAVE_OK');
            $out['message_code'] = 'TXT_SAVE_OK';
        }
        $data['out'] = json_encode($out);
        $this->load->view('JSON',$data);
    }

    /**
     * GET + Headers
     */
    public function listTemplates() {
        if ($this->input->method(true) != 'GET')
            $this->badRequest();
        if (!$this->authenticate())
            $this->forbidden();

        $data['out'] = json_encode($this->Editor->listTemplates(true));
        $this->load->view('JSON',$data);
    }

    public function listTemplatesTime() {
        if ($this->input->method(true) != 'GET')
            $this->badRequest();
        if (!$this->authenticate())
            $this->forbidden();

        $data['out'] = json_encode($this->Editor->listTemplatesTimeEdit(true));
        $this->load->view('JSON',$data);
    }

    /**
     * GET + Headers
     */
    public function getTemplateItems() {
        if ($this->input->method(true) != 'GET')
            $this->badRequest();
        if (!$this->authenticate())
            $this->forbidden();
        //should check if the logged user has access to this template!?
        if(!empty($this->uri->segment(3))) {
            $id = (int)$this->uri->segment(3);
            $data['out'] = "";
			$response = ['success' => false];
            if($id > 0) {
                $templateInfo = $this->Editor->getTemplateInfo($id);
				if(count($templateInfo) > 0) {
					$response = $this->Editor->getTemplateItems($id);
					$idx = count($response);
					if (!empty($templateInfo->bg)) {
						$response[$idx]['bg'] = base_url() . $this->config->item('PATH_ASSSETS') . $templateInfo->bg;
					}
					$tplName = urldecode($templateInfo->name);
					$response[$idx]['time_edit'] = $templateInfo->time_edit;
					$response[$idx]['name'] = $tplName;
					$response[$idx]['portrait'] = $templateInfo->portrait;
					$response[$idx]['thumb'] = base_url() . $this->config->item('PATH_ASSSETS') . preg_replace('/(-|\s|%20)/', "_", $tplName) . ".png";
					$_SESSION['tpl_id'] = $id;
				}
            }
            $data['out'] = json_encode($response);
            $this->load->view('JSON', $data);
        }
    }

    /**
     * add, edit, delete (POST, PUT, DELETE) a template item
     */
    public function item() {
        if (!$this->authenticate(0,1))
            $this->forbidden();
        $tpl_id = (int)$_SESSION['tpl_id'];
        $out['success'] = false;
        $out['time_edit'] = time();
        if($tpl_id < 1) {
            $this->badRequest();
        }
        $data = array_intersect_key($this->dataIn, array_flip($this->fields));

        //if POST add
        if ($this->input->method(true) == 'POST') {
            if (!$this->authenticate('123'))
                $this->forbidden();

            if (empty($data['item']))
                $this->badRequest();

            //should check if it already exists ?
            $out['success_item'] = $this->Editor->addTemplateItem($data, $tpl_id);
            //@todo fix HTML editor position oddity with x/y rel
        }
        //else if PUT update
        elseif ($this->input->method(true) == 'PUT') {
            $item = $data['item'];
            //if not exists - badRequest()
            $itemData = $this->Editor->getTemplateItem($tpl_id, $item);
            if (!is_array($itemData) || count($itemData) == 0)
                $this->badRequest();
            //else update
            $out['success_item'] = $this->Editor->editTemplateItem($data, $item, $tpl_id);
        }
        elseif ($this->input->method(true) == 'DELETE') {
            $out['success_item'] = $this->Editor->deleteTemplateItem($data['item'], $tpl_id);
        }

        //update the template time_edit
        $data = [];
        $data['time_edit'] = time();
        $out['success'] = $this->Editor->editTemplate($data, $_SESSION['tpl_id']);

        $data['out'] = json_encode($out);
        $this->load->view('JSON', $data);
        return $out['success_item'];
    }

}
