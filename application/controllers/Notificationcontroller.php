<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 */
class Notificationcontroller extends MY_Controller {
    public $SITE_DATA;
    protected $fields = array('id', 'src', 'dst', 'title', 'message', 'redirected', 'created', 'was_read');
    protected $fieldsEdit = array('name', 'winner_user_id', 'singles', 'gender', 'level', 'elim_rules', 'courts', 'g_lat', 'g_lon', 'sets', 'city', 'location');
    protected $fields_user = array('user_id1', 'user_id2');
    protected $checkAPIKey = false;
    private $userId = '';

    public function __construct() {
        parent::__construct();

        $this->lang->load('texts_lang');
        $this->lang->load('messages_lang');
        $this->load->model('Notification');
        $this->userId = $this->getLoggedUserId();
        if (empty($this->userId))
            $this->forbidden();
    }

    public function index() {
        redirect('/Welcome/index');
    }

    public function read($id) {
        if (empty($id))
            $this->badRequest();
        $this->JSONOut($this->Notification->read($this->userId, $id));

    }

    public function listAll() {
        $this->JSONOut($this->Notification->getAll($this->userId));
    }

    public function listAllUnread() {
        $this->JSONOut($this->Notification->getAll($this->userId, true));
    }

    public function get($id) {
        $this->JSONOut($this->Notification->get($this->userId, $id));
    }

}