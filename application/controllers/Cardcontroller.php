<?php

defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL);
ini_Set('display_errors', 1);

/**
 * Class Cardcontroller
 * call a test with URL/card/render/(int) size[1,2]/(int) userId/(int) templateId
 *      ex: http://bcapp.ubuntuws/card/render/1/2/1
 * OR:
 * /card/render/1/2/1/1/1 = /card/render/(int) size[1,2]/(int) userId/(int) templateId/(bool) output/(bool) rotate[90deg]
 *  function($size = 1, $userID = NULL, $templateID = NULL, $output = TRUE, $rotate = FALSE)
 */
class Cardcontroller extends MY_Controller
{

	const defaultRatio = .5;
	const defaultWidth = 1800;
	const defaultHeight = self::defaultWidth * self::defaultRatio;
	const defaultPreviewWidth = 900;
	const defaultPreviewHeight = self::defaultPreviewWidth * self::defaultRatio;
	const defaultThumbWidth = 400;
	const defaultThumbHeight = self::defaultThumbWidth * self::defaultRatio;
	const defThumbSuffix = '_thumb';
	const DEBUG_TEXT = FALSE;
	const DEBUG_BOX = FALSE;
	const OVERFLOW_X_BREAK = FALSE; //break line or move on x overflow
	const CACHE_IMAGES = FALSE; //@todo cache images to TRUE in prod
	const defFontSize = 35;

	private $rotate = false;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Editor');
		$this->load->model('User');
	}

	private function _getTemplateItems($templateID)
	{
		$template = array();
		if ($templateID > 0)
		{
			$template = (array) $this->Editor->getTemplateInfo($templateID);
			if (count($template) > 0)
			{
				$items = $this->Editor->getTemplateItems($templateID);
				$template['items'] = $items;
				$template['backgroundImage'] = (!empty($template['bg'])) ?
					base_url() . $this->config->item('PATH_ASSSETS') . $template['bg'] : '';
			}
		}
		return $template;
	}

	private function _hexColorAllocate($image, $hexadecimalValue)
	{
		$hexadecimalValue = ltrim($hexadecimalValue, '#');
		$a = hexdec(substr($hexadecimalValue, 0, 2));
		$b = hexdec(substr($hexadecimalValue, 2, 2));
		$c = hexdec(substr($hexadecimalValue, 4, 2));
		return imagecolorallocate($image, $a, $b, $c);
	}

	private function _getFonts()
	{
		$fonts = $this->Editor->getEnabledFonts();
		$return = array();
		foreach ($fonts as $font)
		{
			$return[$font['name']] = array('R' => $font['regular'], 'B' => $font['bold'], 'I' => $font['italic']);
		}
		return $return;
	}

	private function _writeImage($overwrite, $templateID, $user, $directory, $fileName)
	{
		if ($overwrite || !file_exists($directory . $fileName . '.png'))
		{
			$image = $this->_renderImage($templateID, $user, self::defaultWidth, self::defaultHeight, 1);
			if ($this->rotate)
			{
				$image = imagerotate($image, 90, 0);
			}
			$this->_storePNG_File($image, $directory, $fileName);
			imagedestroy($image);
		}
	}

	private function _writePreviewImage($overwrite, $templateID, $directory, $fileName, $size = 2)
	{
		$suffix = '_preview';
		$height = self::defaultPreviewHeight;
		$width = self::defaultPreviewWidth;
		if ($size == 3)
		{
			$suffix = self::defThumbSuffix;
			$width = self::defaultThumbWidth;
			$height = self::defaultThumbHeight;
		}
		if ($overwrite || !file_exists($directory . $fileName . $suffix))
		{
			if ($this->rotate)
			{
				$image = $this->_resize($directory . $fileName . '.png', $height, $width);
			}
			else
			{
				$image = $this->_resize($directory . $fileName . '.png', $width, $height);
			}
			$this->_storeJPEG_File($image, $directory, $templateID . $suffix);
			imagedestroy($image);
		}
	}

	private function _printPNG($filePath)
	{
		$image = imagecreatefrompng($filePath);
		header("Content-type: image/png");
		imagepng($image);
		imagedestroy($image);
		exit;
	}

	private function _printJPEG($filePath)
	{
		$image = imagecreatefromjpeg($filePath);
		header("Content-type: image/jpeg");
		imagejpeg($image);
		imagedestroy($image);
		exit;
	}

	private function buildUniqueUserDataID($user)
	{
		$uniqueUser = '';
		foreach ($user as $k => $v)
		{
			if (in_array($k, array('id', 'title', 'firstname', 'lastname',
					'company', 'job_title', 'address1', 'address2',
					'postal_code', 'city', 'country', 'phone1',
					'phone2', 'fax', 'email', 'website', 'slogan', 'time_edit')))
			{
				$uniqueUser .= $v;
			}
		}
		return md5($uniqueUser);
	}

	private function _renderSizes($templateID, $size, $userID)
	{
		if ($userID == NULL || ($size < 1 || $size > 3))
		{
			throw new InvalidArgumentException('Invalid arguments provided');
		}
		$user = $this->User->getUserByID($userID);
		$uniqueUserDataID = $this->buildUniqueUserDataID($user);
		if (empty($templateID))
		{
			$templateID = $user['id_template'];
		}
		if (count($user) == 0)
		{
			throw new InvalidArgumentException('Invalid arguments provided');
		}
		$directory = $this->config->item('PATH_CARDS') . $uniqueUserDataID . '/';
		$fileName = $templateID;
		$this->_writeImage(!self::CACHE_IMAGES, $templateID, $user, $directory, $fileName);
		if ($size == 1)
		{
			return $directory . $fileName . '.png';
		}
		else if ($size == 2)
		{
			$this->_writePreviewImage(!self::CACHE_IMAGES, $templateID, $directory, $fileName);
			return $directory . $fileName . '_preview.jpg';
		}
		else if ($size == 3)
		{
			$this->_writePreviewImage(!self::CACHE_IMAGES, $templateID, $directory, $fileName, $size);
			return $directory . $fileName . self::defThumbSuffix . '.jpg';
		}
		return '';
	}

	public function render($size = 1, $userID = NULL, $templateID = NULL, $output = TRUE, $rotate = FALSE)
	{
		$output = (bool) $output;
		$this->rotate = (bool) $rotate;
		$filePath = $this->_renderSizes($templateID, $size, $userID);
		if ($size == 1 && $output === TRUE)
		{
			$this->_printPNG($filePath);
		}
		else if (($size == 2 || $size == 3) && $output === TRUE)
		{
			$this->_printJPEG($filePath);
		}
		$cards = array();
		$cards[$templateID] = $filePath;
		$data['out'] = json_encode($cards);
		header('Content-Type: application/json'); //@TODO maybe set this for other json-'views' too?
		$this->load->view('JSON', $data);
	}

	private function _resize($bigFile, $width, $height)
	{
		list($srcWidth, $srcHeight) = getimagesize($bigFile);
		$im = imagecreatefromstring(file_get_contents($bigFile));
		$image = imagecreatetruecolor($width, $height);
		imagepalettecopy($image, $im);
		imagecopyresampled($image, $im, 0, 0, 0, 0, $width, $height, $srcWidth, $srcHeight);
		return $image;
	}

	public function renderAll($userID)//@todo only logged in user may trigger this, because the operations are cpu intensive
	{
		$templates = $this->Editor->getAllTemplates();
		if (count($templates) == 0)
		{
			throw new Exception('No templates configured');
		}
		$cards = array();
		foreach ($templates as $template)
		{
			$cards[$template['id']]['full'] = $this->_renderSizes($template['id'], 1, $userID);
			$cards[$template['id']]['preview'] = $this->_renderSizes($template['id'], 2, $userID);
		}
		$data['out'] = json_encode($cards);
		header('Content-Type: application/json');
		$this->load->view('JSON', $data);
	}

	private function _storeJPEG_File($image, $directory, $fileName)
	{
		$cardFile = $directory . $fileName . ".jpg";
		if (!file_exists($directory) && !is_dir($directory))
		{
			mkdir($directory, 0755, true);
		}
		imagejpeg($image, $cardFile, 82);
		chmod($cardFile, 0644);
		return $cardFile;
	}

	private function _storePNG_File($image, $directory, $fileName)
	{
		$cardFile = $directory . $fileName . ".png";
		if (!file_exists($directory) && !is_dir($directory))
		{
			mkdir($directory, 0755, true);
		}
		imagepng($image, $cardFile, 9);
		chmod($cardFile, 0644);
		return $cardFile;
	}

	private function _renderImage($templateID, $user, $width, $height)
	{
		if (!function_exists("bcscale"))
		{
			return;
		}
		$textBaseSize = self::defFontSize;
		$baseWidth = 510;
		$baseHeight = 330;
		$template = $this->_getTemplateItems($templateID);
		if (count($template) == 0)
		{
			throw new InvalidArgumentException('Invalid arguments provided');
		}
		$fonts = $this->_getFonts();
		if (count($fonts) == 0)
		{
			throw new InvalidArgumentException('Invalid arguments provided');
		}
		$image = imagecreatetruecolor($width, $height);
		$background = !empty($template['backgroundImage']) ? ($template['backgroundImage']) : '';
		if (empty($background))
			$background = (file_exists(base_url() . $this->config->item('PATH_ASSSETS') . 'default.png')) ?
				(base_url() . $this->config->item('PATH_ASSSETS') . 'default.png') : '';

		$fillBg = true;
		if (!empty($background))
		{
			$fillBg = false;
			$imageSize = @getimagesize($background);
			if ($imageSize === FALSE)
			{
				$fillBg = true;
			}
			list($srcWidth, $srcHeight) = $imageSize;
			$imageFromString = imagecreatefromstring(file_get_contents($background));
			imagepalettecopy($image, $imageFromString);
			imagecopyresampled($image, $imageFromString, 0, 0, 0, 0, $width, $height, $srcWidth, $srcHeight);
		}
		if ($fillBg)
		{
			$background_color = imagecolorallocate($image, 255, 255, 255);
			imagefill ( $image , 0 , 0 , $background_color);
		}
		imagesetthickness($image, 2);
		bcscale(12);
		foreach ($template['items'] as $item)
		{
			$fontSize = $textBaseSize;
			$text = $user[$item['item']];

			if (self::DEBUG_TEXT && $text == '')
			{
				$text = $item['item'];
			}
			$fontType = 'R';
			if ($item['bold'])
			{
				$fontType = 'B';
			}
			if ($item['italic'])
			{
				$fontType = 'I'; //@TODO support italic fonts, then we need separate italic-fonts
			}
			if ($item['font_name'] == '')
			{
				$item['font_name'] = 'Roboto';
			}
			if (!isset($fonts[$item['font_name']]))
			{
				throw new Exception('Could not understand font name "' . $item['font_name'] . '".');
			}
			if (!isset($fonts[$item['font_name']][$fontType]) || $fonts[$item['font_name']][$fontType] == '')
			{
				//@TODO here it now renders font instead of italic|bold as regular if font does not support it
				$fontType = 'R';
			}
			$font = $fonts[$item['font_name']][$fontType];
			if ($item['font_size'] != "")
			{
				if ($item['font_size'] == 0)
				{
					$item['font_size'] = 100;
				}
				$fontSize *= $item['font_size'] / 100;
			}
			if ($item['color'] == "")
			{
				$textColor = imagecolorallocate($image, 125, 135, 188);
			}
			else
			{
				$textColor = $this->_hexColorAllocate($image, $item['color']);
			}
			$x = bcmul($item['x_rel'], bcdiv($width, $baseWidth));
			$y = bcmul($item['y_rel'], bcdiv($height, $baseHeight));
			$fontFullPath = $this->config->item('PATH_FONTS') . $font;

			if (self::OVERFLOW_X_BREAK)
			{
				list($text, $box, $boxColor) = $this->_breakTextOnOverflow($text, $x, $image, $width, $fontFullPath, $fontSize);
			}
			else
			{
				list($box, $boxColor, $x, $y) = $this->_moveTextOnOverflow($text, $x, $y, $image, $width, $fontFullPath, $fontSize);
			}
			if (self::DEBUG_BOX)
			{
				imagerectangle($image, $x + $box[6], $y + $box[7], $x + $box[2], $y + $box[3], $boxColor);
			}
			imagettftext($image, $fontSize, 0, $x, $y, $textColor, $fontFullPath, $text);
			imagecolordeallocate($image, $textColor);
		}
		return $image;
	}

	private function _moveTextOnOverflow($text, $x, $y, $image, $width, $fontFullPath, $fontSize)
	{
		$textLength = strlen($text);
		$originalText = $text;
		$textFits = TRUE;

		do
		{
			$box = imagettfbbox($fontSize, 0, $fontFullPath, $text);
			$boxWidth = abs($box[4] - $box[0]);
			//$boxHeight = abs($box[5] - $box[1]); // could decrease font-size until fit, should include width then too
			if ($boxWidth + $x > $width) //only breaks line if does not fit into given width box
			{
				$textLength--;
				$textFits = FALSE;
				$boxColor = imagecolorallocate($image, 255, 0, 0);
				$text = substr($originalText, 0, $textLength) . "\n" . substr($originalText, $textLength);
			}
			else
			{
				$boxColor = imagecolorallocate($image, 0, 255, 0);
				if (!$textFits)
				{
					$textBoxCut = imagettfbbox($fontSize, 0, $fontFullPath, substr($originalText, $textLength));
					$textBoxCutWidth = abs($textBoxCut[4] - $textBoxCut[0]);
					$x -= $textBoxCutWidth;
					$box[2] += $textBoxCutWidth;
				}
				break;
			}
		}
		while ($textLength > 0);

		return array($box, $boxColor, $x, $y);
	}

	private function _breakTextOnOverflow($text, $x, $image, $width, $fontFullPath, $fontSize)
	{
		$textLength = strlen($text);
		$originalText = $text;
		do
		{
			$box = imagettfbbox($fontSize, 0, $fontFullPath, $text);
			$boxWidth = abs($box[4] - $box[0]);
			//$boxHeight = abs($box[5] - $box[1]); // could decrease font-size until fit, should include width then too
			if ($boxWidth + $x > $width) //only breaks line if does not fit into given width box
			{
				$textLength--;
				$boxColor = imagecolorallocate($image, 255, 0, 0);
				$text = substr($originalText, 0, $textLength) . "\n" . substr($originalText, $textLength);
			}
			else
			{
				$boxColor = imagecolorallocate($image, 0, 255, 0);
				break;
			}
		}
		while ($textLength > 0);
		return array($text, $box, $boxColor);
	}

	private function _oldExample()
	{
		/* $textSize = 25;
		  $image = imagecreatefrompng($template['backgroundImage']);
		  //$image = imagecreate(850, 550);
		  $font = 'public/fonts/RobotoCondensed-Regular.ttf';
		  $fontB = 'public/fonts/RobotoCondensed-Bold.ttf';
		  $font2 = 'public/fonts/GreatVibes-Regular.ttf';
		  $background = imagecolorallocate($image, 0, 0, 255);
		  $text_colour = imagecolorallocate($image, 125, 135, 188);
		  $text_colour2 = imagecolorallocate($image, 102, 0, 51);
		  $txtOrg = imagecolorallocate($image, 204, 51, 0);
		  $line_colour = imagecolorallocate($image, 128, 255, 0);
		  imagettftext($image, ($textSize + 2), 90, 33, 355, $text_colour, $fontB, "Cristian Hieronimus Bell");
		  imagettftext($image, $textSize, 0, 40, 30, $text_colour2, $font2, "postman on");
		  imagettftext($image, $textSize, 0, 40, 320, $text_colour, $font, "postman@mailforpostman.de");
		  $text = "http://super-postman.ca";
		  $sz1 = imagettfbbox($textSize, 0, $font, $text);
		  imagettftext($image, 28, 0, 40, 360, $text_colour2, $font, $text . "_" . ($sz1[2] - $sz1[0]));
		  imagettftext($image, ($textSize + 2), 0, 500, 430, $txtOrg, $font2, "+49.172.7778738");
		  $text = "London, Otario";
		  $sz2 = imagettfbbox($textSize + 2, 0, $font, $text);
		  imagettftext($image, ($textSize + 2), 0, 500, 460, $txtOrg, $font2, $text . "_" . ($sz2[2] - $sz2[0]));
		  imagesetthickness($image, 5);
		  imageline($image, 40, 45, 165, 45, $line_colour);
		  header("Content-type: image/png");
		  imagepng($image);
		  imagecolordeallocate($line_color);
		  imagecolordeallocate($text_color);
		  imagecolordeallocate($background);
		  imagedestroy($image);
		  exit; */
	}

}

