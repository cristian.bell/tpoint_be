</div>
<script src="<?PHP echo base_url()?>public/js/jquery-2.2.4.min.js"></script>
<script src="<?PHP echo base_url()?>public/js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script src="<?PHP echo base_url()?>public/js/functions.editor.js"></script>
<script language="javascript" type="application/javascript">
    editor.tpl_name = "<?=$tpl->name;?>";
    editor.tpl_name_tmp = editor.tpl_name;
    $(document).ready(function () {
        $canvas = $("#canvas");
        $menu = $("#menu");
        <?php
            foreach ($fonts as $key => $value) {
        ?>editor.fonts[<?=$key;?>]='<?=$value;?>';<?php
        }?>
        var thisNr = 0;<?php
        if(isset($_SESSION['tpl_id']) && !empty((int)$_SESSION['tpl_id']))
            echo "thisNr=".$_SESSION['tpl_id'].";";?>

        if(editor.tpl_name != '' && (typeof thisNr == "number" && thisNr > 0)){
            $(".menu").show();
            $canvas.show();
            drawCanvas({}, thisNr, editor.tpl_name);
        }
        $(".name_save").on("click", saveTemplateName);
        $(".name_cancel, .new_tpl").on("click", dialogNewTemplate);

        $(".sw_bg, .sw_items").on("click", switchTabs);
        $(".bold, .italic").on("click", setItemFormat);
        $menu.children().on("click", moveToCanvas).on("mouseenter", function () {
            if (typeof $(this).attr("id") == "string" && $(this).attr("id") != "")
                outputInfo('<?=$this->lang->line('ED_ME_ITEM_OVER');?>');
        }).on("mouseleave", function () {
            outputInfo('<?=$this->lang->line('');?>');
        });
        $canvas.children().on("click", moveToCanvas).on("mouseenter", function () {
            outputInfo('<?=$this->lang->line('ED_ME_ITEM_OVER');?>');
        }).on("mouseleave", function () {
            outputInfo('');
        });

        $(".name > input").on("click", function () {
            outputInfo('<?=$this->lang->line('ED_TITLE_BLUR');?>');
        }).on("blur", function () {
            editor.tpl_name = $(this).val().replace(/[^a-zA-Z0-9\-\_\ ]/g, '');
            //console.log(name);
            if(editor.tpl_name.length>0 && editor.tpl_name != editor.tpl_name_tmp){
                $(".name_save").show();
            }else{
                $(".name_save").hide();
            }
            outputInfo('');
        });

        $(".menu .backgrounds > img").on("click", function () {
            applyBackground($(this).attr("src"));
        });

        $( ".text_size" ).slider({
            value:100,
            min: 75,
            max: 200,
            step: 25,
            disabled: true,
            change: function( event, ui ) {
                sliderItemSize(event, ui);
            },
            slide: function( event, ui ) {
                sliderItemSize(event, ui);
            }
        });
        $( ".font_name" ).slider({
            value:2,
            min: 1,
            max: (editor.fonts.length-1),
            step: 1,
            disabled: true,
            change: function( event, ui ) {
                sliderItemFont(event, ui);
            },
            slide: function( event, ui ) {
                sliderItemFont(event, ui);
            }
        });
        $( ".text_color" ).slider({
            value:0,
            min: 0,
            max: (editor.colors.length-1),
            step: 1,
            disabled: true,
            change: function( event, ui ) {
                sliderItemColor(event, ui);
            },
            slide: function( event, ui ) {
                sliderItemColor(event, ui);
            }
        });

        $("#all_templates").on("change", drawCanvas);

        $(this).keydown(function (ev) {
            var x = ev.which || ev.keyCode;
            handleKeys(x);
        });
        //console.log(editor.fonts);
        texts['ED_CA_ITEM_OVER'] = '<?=$this->lang->line('ED_CA_ITEM_OVER');?>';
        texts['ED_CA_ITEM_MOVE'] = '<?=$this->lang->line('ED_CA_ITEM_MOVE');?>';
    });
</script>
<div class="mask"></div>
</body>
</html>

