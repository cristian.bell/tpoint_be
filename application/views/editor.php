<style type="text/css">
    .ui-widget {
        font-family: Verdana,Arial,sans-serif;
        font-size: 0.8em;
    }
</style>
<div id="all">
    <div id="" class="pageHeader">some header up here<div><select id="all_templates"><?php
                /*<option value="0">-- --</option><?php*/
                foreach($templates as $id=>$name){?><option value="<?=$id;?>" <?php if($tpl_id == $id) echo "selected";?>><?=$name;?></option><?php }?>
        </select></div>
        <div class="new_tpl btn btn-primary"><?=$this->lang->line('LBL_NEW');?></div></div>
    <div class="divO menu">
        <div class="menu_switcher">
            <div class="sw_items">Items</div>
            <div class="sw_bg">Backgrounds</div>
        </div>
        <div id="menu">
            <div id="firstname" class="divU item required"
                 >Given Name</div>
            <div id="lastname" class="divU item required"
                 >Surname</div>
            <div id="email" class="divU item required"
                 >e-mail@mail.com</div>
            <div id="website" class="divU item"
                 >https://website.tld</div>
            <div id="title" class="divU item"
                 >Dr/Mr/Sir</div>
            <div id="job_title" class="divU item required"
                 >job_title</div>
            <div id="company" class="divU item"
                 >company</div>
            <div id="phone1" class="divU item required"
                 >+123.12345.one</div>
            <div id="phone2" class="divU item"
                 >+123.22225.two</div>
            <div id="fax" class="divU item"
                 >+123.22225.fax</div>
            <div id="address1" class="divU item"
                 >10 Downing Street</div>
            <div id="address2" class="divU item"
                 >(Mr Janitor)</div>
            <div id="postal_code" class="divU item"
                 >SW1A 2AA</div>
            <div id="city" class="divU item"
                 >London</div>
            <div id="country" class="divU item"
                 >Great Empire</div>
            <div id="slogan" class="divU item"
                 >slogan</div>
        </div>
        <div class="tools">
            <div class="bold"><?=$this->lang->line('ED_ME_BOLD_LBL');?></div>
            <div class="italic"><?=$this->lang->line('ED_ME_ITALIC_LBL');?></div>
            <?=$this->lang->line('ED_ME_SIZE_LBL');?>:&nbsp;<span id="text_size">100%</span>
            <div id="" class="text_size"></div>
            <?=$this->lang->line('ED_ME_FONT_LBL');?>:&nbsp;<span id="font_name">Roboto</span>
            <div id="" class="font_name"></div>
            <?=$this->lang->line('ED_ME_COLOR_LBL');?>:&nbsp;<span id="text_color"></span>
            <div class="text_color"></div>
        </div>
        <div class="backgrounds">
            <img src="<?=base_url();?>public/assets/background_1.png">
            <img src="<?=base_url();?>public/assets/background_1bw.png">
            <img src="<?=base_url();?>public/assets/background_2.png">
            <img src="<?=base_url();?>public/assets/background_2bw.png">
            <img src="<?=base_url();?>public/assets/background_3.png">
            <img src="<?=base_url();?>public/assets/background_3bw.png">
            <img src="<?=base_url();?>public/assets/background_4.png">
            <img src="<?=base_url();?>public/assets/background_4bw.png">
            <img src="<?=base_url();?>public/assets/background_5.png">
            <img src="<?=base_url();?>public/assets/background_5bw.png">
        </div>
    </div>
    <div id="" class="main">
        <div id="" class="name"><input type="text" maxlength="25" placeholder="Template name" id="tpl_title"
                                       value="<?=($tpl->name);?>"/>
            <span class="name_save btn btn-success"><?=$this->lang->line('LBL_SAVE');?></span>
            <span class="name_cancel btn btn-warning"><?=$this->lang->line('LBL_CANCEL');?></span></div>
        <div id="" class="divO info"></div>
        <div id="canvas" class="canvas"
             <?php /*?>ondragover="allowDrop(event)" ondrop="dropInCanvas(event)"><?php*/ ?>></div>
    </div>
</div>

<script language="javascript" type="application/javascript">
</script>

<?php

?>