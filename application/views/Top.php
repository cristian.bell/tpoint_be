<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" <?php /*"http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"*/ ?>>
<html itemscope itemtype="http://schema.org/Article" xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://opengraphprotocol.org/schema/"
      xml:lang="de" xmlns="http://www.w3.org/1999/html">
<head>
	<title>BCApp</title>

	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="expires" content="3600" />

	<meta name="robots" content="index,no-follow" />
	<meta name="description" content="" />
	<meta name="author" content="Creative AdIT+CristianBell" />
	<meta name="keywords" content="" />
	<meta name="date" content="2016-15-02T09:43:23+02:00" />
	<meta name="MSSmartTagsPreventParsing" content="TRUE" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="ROBOTS" content="NOARCHIVE" /><?php
	if(isset($editorMode) && $editorMode){?>
    <link rel="stylesheet" href="<?PHP echo base_url()?>public/js/jquery-ui-1.11.4.custom/jquery-ui.min.css">
	<link rel="stylesheet" href="<?PHP echo base_url()?>public/js/bootstrap-3.3.5-dist/css/bootstrap.css"><?php }?>
    <link rel="stylesheet" href="<?php echo base_url()?>public/css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Syncopate:400,700|Roboto:400,700|Droid+Sans:400,700' rel='stylesheet' type='text/css'>
</head>

<body >
<div class="wrapper">
<div class="header"><a href="<?=base_url()?>">
		<img src="<?PHP echo base_url()?>public/img/logo.png" style="height: 110px;"/></a>
    <br/><div style=""><?=$headerinfo;?></div></div>