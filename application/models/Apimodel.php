<?php

class Apimodel extends MY_Model
{

	private $db;

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->_loadDatabase('pdo');
	}

	public function editShortUser($userID, $title, $firstName, $lastName, $jobTitle, $phone1, $email)
	{
		$stmt = $this->db->prepare("
			UPDATE users 
			SET title=:title,firstname=:firstName,lastname=:lastName,job_title=:jobTitle,phone1=:phone1,email=:email
			WHERE id=:userID");
		$stmt->bindParam(':userID', $userID);

		$stmt->bindParam(':title', $title);
		$stmt->bindParam(':firstName', $firstName);
		$stmt->bindParam(':lastName', $lastName);
		$stmt->bindParam(':jobTitle', $jobTitle);
		$stmt->bindParam(':phone1', $phone1);
		$stmt->bindParam(':email', $email);
		return $stmt->execute();
	}

	public function editUser($userID, $title, $firstName, $lastName, $jobTitle, $phone1, $email, $address1, $address2, $postalCode, $city, $country, $company, $phone2, $fax, $website, $slogan)
	{
		$stmt = $this->db->prepare("
			UPDATE users 
			SET title=:title,firstname=:firstName,lastname=:lastName,company=:company,job_title=:jobTitle,address1=:address1,address2=:address2,postal_code=:postalCode,
			city=:city,country=:country,phone=:phone,fax=:fax,email=:email,website=:website,slogan=:slogan 
			WHERE id=:userID");
		$stmt->bindParam(':userID', $userID);

		$stmt->bindParam(':title', $title);
		$stmt->bindParam(':firstName', $firstName);
		$stmt->bindParam(':lastName', $lastName);
		$stmt->bindParam(':company', $company);
		$stmt->bindParam(':jobTitle', $jobTitle);
		$stmt->bindParam(':address1', $address1);
		$stmt->bindParam(':address2', $address2);
		$stmt->bindParam(':postalCode', $postalCode);
		$stmt->bindParam(':city', $city);
		$stmt->bindParam(':country', $country);
		$stmt->bindParam(':phone1', $phone1);
		$stmt->bindParam(':phone2', $phone2);
		$stmt->bindParam(':fax', $fax);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':website', $website);
		$stmt->bindParam(':slogan', $slogan);
		return $stmt->execute();
	}

	public function createUser($title, $firstName, $lastName, $jobTitle, $phone1, $email, $address1, $address2, $postalCode, $city, $country, $company, $phone2, $fax, $website, $slogan,$pin)
	{
		$templateID = 1; //@TODO set default or parameter
		$image = ''; //@TODO set default or parameter
		$key = ''; //@TODO set default

		$stmt = $this->db->prepare("
			INSERT INTO users(`title`,`firstname`,`lastname`,`company`,`job_title`,`address1`,`address2`,`postal_code`,
	`city`,`country`,`phone1`,`phone2`,`fax`,`email`,`website`,`slogan`,
	`id_template`,`image`,`pin`,`key`) 
			VALUES (:title, :firstName, :lastName,:company,:jobTitle,:address1,:address2,:postalCode,
			:city,:country,:phone1,:phone2,:fax,:email,:website,:slogan,
			:templateID,:image,:pin,:key)");
		$stmt->bindParam(':title', $title);
		$stmt->bindParam(':firstName', $firstName);
		$stmt->bindParam(':lastName', $lastName);
		$stmt->bindParam(':company', $company);
		$stmt->bindParam(':jobTitle', $jobTitle);
		$stmt->bindParam(':address1', $address1);
		$stmt->bindParam(':address2', $address2);
		$stmt->bindParam(':postalCode', $postalCode);
		$stmt->bindParam(':city', $city);
		$stmt->bindParam(':country', $country);
		$stmt->bindParam(':phone1', $phone1);
		$stmt->bindParam(':phone2', $phone2);
		$stmt->bindParam(':fax', $fax);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':website', $website);
		$stmt->bindParam(':slogan', $slogan);
		$stmt->bindParam(':templateID', $templateID);
		$stmt->bindParam(':image', $image);
		$stmt->bindParam(':pin', $pin);
		$stmt->bindParam(':key', $key);
		if ($stmt->execute())
		{
			return ['userID' => $this->db->lastInsertId(), 'key' => $key];
		}
		return NULL;
	}

	public function updateLocation($userID, $latitude, $longitude)
	{
		$stmt = $this->db->prepare("
			INSERT INTO users_geo 
			(userID, latitude, longitude) 
			VALUES (:userID, :latitude, :longitude)");
		$stmt->bindParam(':userID', (int)$userID);
		$stmt->bindParam(':latitude', (float)$latitude);
		$stmt->bindParam(':longitude', (float)$longitude);
		return $stmt->execute();
	}

	public function getNearbyUsers($maxDistance, $latitude, $longitude,$start=0, $limit=50)
	{
		$stmt = $this->db->prepare("
			SELECT userID, title, firstname, lastname,email,latitude, longitude,modifiedAt, 
			( 6371 * acos(cos(radians(:latitude))*cos(radians(latitude))*cos(radians(longitude) - radians(:longitude))+sin(radians(:latitude)) * sin( radians(latitude) ) ) ) AS distance
			FROM users_geo g JOIN users u ON g.userID=u.id
			HAVING distance <= :maxDistance
			ORDER BY distance ASC
			LIMIT :start,:limit");
		$stmt->bindParam(':maxDistance', (int)$maxDistance);
		$stmt->bindParam(':latitude', (float)$latitude);
		$stmt->bindParam(':longitude', (float)$longitude);
		$stmt->bindParam(':limit', (int)$limit);
		$stmt->bindParam(':start', (int)$start);
		$stmt->execute();
		return $stmt->fetchAll();
	}

}
