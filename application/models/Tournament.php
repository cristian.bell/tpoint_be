<?php
class Tournament extends MY_Model {

	protected $main_tbl = 'tournaments';
	protected $linked_tbl = 'users_tournaments';
    protected $user_tbl = 'users';
	protected $all_fields = '*';
	protected $min_fields = 'id, name, gender, singles, level, elim_rules, courts, sets, start_date, end_date, location, creator_user_id';
	protected $nr_players_subq = '(SELECT COUNT(user_id) FROM users_tournaments WHERE tournament_id = t.id) AS nr_players';
	protected $linked_fields_cnt = ', COUNT(u.user_id) AS nr_players';
	protected $future_only_cond = 't.end_date > NOW()';
	public $usrImgPathWeb = '';

	public function __construct()
	{
		parent::__construct();
	}

	public function add($data) {
	    $this->prepOutputData($data);
        $data['name'] = $this->db->escape($data['name']);

		$return = false;
		$success = $this->db->insert($this->main_tbl, $data);
		if ($success)
			$return = $this->db->insert_id();
		return $return;
	}

    public function edit($data, $id) {
        $this->prepOutputData($data);

        $this->db->where('id', $id);
        $data['name'] = $this->db->escape($data['name']);
        $success = $this->db->update($this->main_tbl, $data);

        if($success)
            return $success;
        else
            return false;
    }

    public function deactivate($id) {
        $this->db->where('id', $id);
        $success = $this->db->update($this->main_tbl, array('disabled' => 1));
        if($success)
            return $success;
        else
            return false;
    }

	public function getAll($futureOnly = false, $full = false) {
		$fields = $this->all_fields;
		if (!$full || $futureOnly)
			$fields = $this->min_fields;

        $query = $this->db->select($fields. ', ' . $this->nr_players_subq . '')
			->from($this->main_tbl.' t')
			->order_by('t.start_date', 'DESC');
		if ($futureOnly)
			$query->where($this->future_only_cond);

		$result = $query->get()->result_array();
		//var_dump($this->db->last_query());

        foreach ($result as $key => $item) {
            if (empty($result[$key]['start_date'])) {
                unset($result[$key]);
            } else {
                $result[$key]['start_date'] = $this->formatOutputDate($item['start_date']);
                $result[$key]['end_date'] = $this->formatOutputDate($item['end_date']);
                //unset($result[$key]['creator_user_id']);
            }
        }
		return $result;
	}

    public function getAllPairs() {
        $query = $this->db->select($this->min_fields)
            ->from($this->main_tbl)
            ->get();
        $result = $query->result_array();
        $out = array();
        foreach ($result as $key => $item) {
            $out[$result[$key]['id']] = array(
                'name' => $result[$key]['name'],
                'start' => $this->formatOutputDate($result[$key]['start_date']),
                'end' => $this->formatOutputDate($result[$key]['end_date']),
                'location' => $result[$key]['location'],
            );
        }
        return $out;
    }

    public function isCreator($userID, $tournamentID):bool {
        $query = $this->db->select('COUNT(*)')
            ->from($this->main_tbl.' t')
            ->where(array('creator_user_id' => $userID, 'id' => $tournamentID))
            ->get();
        $result = $query->row_array();
        return (bool)$result;
    }

    public function join($data) {
        return $this->db->insert($this->linked_tbl, $data);
    }

    public function leave($data) {
        return $this->db->delete($this->linked_tbl, $data);
    }

	public function getAllByUser($userID, $futureOnly = false, $full = true) {
        $fields = $this->all_fields;
        if (!$full)
            $fields = $this->min_fields;

        $query = $this->db->select($fields. ', ' . $this->nr_players_subq . '')
            ->from($this->main_tbl.' t')
            ->where('ut.user_id', $userID)
            ->join($this->linked_tbl.' ut', 't.id = ut.tournament_id', 'left');

        if ($futureOnly)
			$query->where($this->future_only_cond);

		$result = $query->get()->result_array();
		foreach ($result as $key => $item) {
            $result[$key]['start_date'] = $this->formatOutputDate($item['start_date']);
            $result[$key]['end_date'] = $this->formatOutputDate($item['end_date']);
		}

		return $result;
	}

	public function getByID($id, $full = true):array {
		$fields = $this->all_fields;
		if (!$full)
			$fields = $this->min_fields;

        $query = $this->db->select($fields. ', ' . $this->nr_players_subq . '')
			->from($this->main_tbl.' t')
			->where('id', $id)
			->get();
		$result = $query->row_array();
		if (empty($result) || count($result) == 0)
		    return array();

        $query = $this->db->select('u.id, u.lastname, u.firstname, u.photo ' . '')
            ->from($this->user_tbl.' u')
            ->where('ut.tournament_id', $id)
            ->join($this->linked_tbl.' ut', 'ut.user_id = u.id', 'left');
        $result['users'] = $query->get()->result_array();
        foreach ($result['users'] as $key => $value) {
            if (!empty($result['users'][$key]['photo']))
                $result['users'][$key]['photo'] = $this->usrImgPathWeb . $result['users'][$key]['photo'];
        }

        $result['start_time'] = strtotime($result['start_date']);
        $result['start_date'] = $this->formatOutputDateMonth($result['start_date']);
        $result['end_date'] = $this->formatOutputDateMonth($result['end_date']);

		return $result;
	}




	public function deleteMatchUsers($matchID) {
		$this->db->delete($this->tbl_users_matches, array('match_id' => $matchID));
	}

	private function prepOutputData(&$objData) {
        if (empty($objData['singles']))
            $objData['singles'] = 1;
        if (empty($objData['gender']))
            $objData['gender'] = 1;
        if (empty($objData['elim_rules']))
            $objData['elim_rules'] = 1;
        //@todo remove above after alpha
        $objData['sets'] = 1;
        $objData['location'] = (int)$objData['location'];
        $objData['singles'] = (int)$objData['singles'];
        $objData['gender'] = (int)$objData['gender'];
        $objData['level'] = (float)$objData['level'];
        $objData['elim_rules'] = (int)$objData['elim_rules'];
        if (isset($objData['courts']))
            $objData['courts'] = (int)$objData['courts'];
        else
            $objData['courts'] = 0;
        //$objData['sets'] = (int)$objData['sets'];
        unset($objData['g_lat']);
        unset($objData['g_lon']);
    }


}

