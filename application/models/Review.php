<?php
class Review extends MY_Model {

    protected $main_tbl = 'reviews';
    protected $main_tbl_short = ' t';
    protected $linked_tbl = '';
    protected $linked_tbl_short = ' m';
    protected $all_fields = 't.*';
    protected $min_fields = 'user_id_writer, match_id, u_character, u_technique, manner, punctuality, power, 
    consistency, strategy, review_text, time_edit';
    //protected $unread_only_cond = "t.id NOT IN (SELECT notification_id FROM users_notifications WHERE user_id = '";
    protected $def_cond = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function get(string $userID, string $id) {
        $fields = $this->all_fields;
        $this->load->model('User');
        $users = $this->User->getAllPairs();

        $query = $this->db->select($fields)
            ->from($this->main_tbl . $this->main_tbl_short)
            ->where(array('user_id' => $userID, 'id' => $id))
            ->order_by('time_edit', 'DESC');

        $results = $query->get()->result_array();

        $this->prepareDataOut($results, $users);
        return $results;
    }

    public function getAll(string $userID) {
        $fields = $this->all_fields;
        $this->load->model('User');
        $users = $this->User->getAllPairs();

        $query = $this->db->select($fields)
            ->from($this->main_tbl . $this->main_tbl_short)
            ->where('user_id', $userID)
            ->order_by('time_edit', 'DESC');

        $results = $query->get()->result_array();

        $this->prepareDataOut($results, $users);
        return $results;
    }

    public function add(array $data) {
        $return = false;
        $this->prepareDataIn($data);
        if (isset($data['review_text']))
            $data['review_text'] = $this->db->escape($data['review_text']);

        $success = $this->db->insert($this->main_tbl, $data);
        if ($success)
            $return = $this->db->insert_id();
        return array('success' => $success, 'id' => $return);
    }

    private function prepareDataIn(&$data) {
        $data['match_id'] = (int)$data['match_id'];
        $data['manner'] = (int)$data['manner'];
        $data['punctuality'] = (int)$data['punctuality'];
        $data['power'] = (int)$data['consistency'];
        $data['strategy'] = (int)$data['strategy'];
        $data['consistency'] = (int)$data['consistency'];
    }

    private function prepareDataOut(&$data, &$users) {
        foreach ($data as $key => $item) {
            $data[$key]['created'] = $this->formatOutputDate($item['time_edit']);
            if (key_exists($item['user_id_writer'], $users)) {
                $data[$key]['user_name_writer'] = $users[$data[$key]['user_id_writer']];
            } else
                $data[$key]['user_name_writer'] = 'Some user';

            $data[$key]['character'] = $data[$key]['u_character'];
            $data[$key]['technique'] = $data[$key]['u_technique'];
            unset($data[$key]['u_character']);
            unset($data[$key]['u_technique']);
            unset($data[$key]['time_edit']);
            unset($data[$key]['user_id']);
        }
    }


}


