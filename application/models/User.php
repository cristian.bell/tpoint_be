<?php
class User extends MY_Model {

    protected $tbl_users = 'users';
	protected $tbl_users_matches = 'users_matches';
	protected $tbl_matches = 'matches';
	protected $tbl_reviews = 'reviews';
	protected $some_fields = 'firstname, lastname, email, phone, show_email, show_phone, photo, gender, age, city, country, 
    		level, dominant_hand, backhand_style, play_times, play_years, gear_1, main_skill, main_skill2, last_login';
	protected $min_fields = 'firstname, lastname, id';

	protected $p_cost = 12;

	public function __construct()
	{
		parent::__construct();
		$this->p_cost = $this->config->item('MIN_USER_ID_LEN');
	}

    /**
     * saves the current user info
     * @return mixed
     */
	public function addUser($data) {
		$query = $this->db->select('id')
			->from($this->tbl_users)
			->get();

		$data['pass'] = $this->uPassword($data['pass']);
		$data['id'] = $this->User->generatePin(false, array_map('current', $query->result_array()));
		if (isset($data['level']))
			$data['level'] = $this->levelFromAPI($data['level']);

		$success = $this->db->insert($this->tbl_users, $data);
        if($success)
            return $data['id'];
        else
            return false;
	}

    /**
     * edits user information
     * @param $data
     * @param $id
     * @return bool
     */
    public function editUser($data, $id) {
		if (isset($data['level']))
			$data['level'] = $this->levelFromAPI($data['level']);

        $this->db->where('id', $id);
        $success = $this->db->update($this->tbl_users, $data);

        if ($success)
            return $success;
        else
            return false;
    }

    public function addPhoto($data, $id) {
        $this->db->where('id', $id);
        return $this->db->update($this->tbl_users, $data);
    }

	public function getUserByMailPass($email, $pass, $filePath, $filePathStore = null) {
		$result = null;
		$query = $this->db->select('*')
			->from($this->tbl_users)
			->where(array('email' => $email))
			->get();
		$result = $query->row_array();
		if (password_verify($pass, $result['pass'])) {
			unset($result['pass']);
			$result['last_login'] = $this->formatOutputDate($result['last_login']);
			$this->photoToApi($result, $filePath, $filePathStore);
		}
		else
			$result = null;

		return $result;
	}

    /**
     * returns the e-mail address for a given user id
     * @param $uid
     * @return null
     */
    public function getMail($id) {
        $query = $this->db->select('email')
            ->from($this->tbl_users)
            ->where('id', (int) $id)
            ->get();

        $result = $query->row();
        if (is_object($result))
            return $result->email;
        else
            return null;
    }

    public function getUserByMail($email, $filePath = null, $filePathStore = null) {
    	$result = null;
		$query = $this->db->select('*')
			->from($this->tbl_users)
			->where('email', $email)
			->get();
		$result = $query->row_array();
		unset($result['pass']);
		unset($result['id']);
        $this->photoToApi($result, $filePath, $filePathStore);
		return $result;
	}

	public function getUserByID($userID, $full = false, $filePath = null, $filePathStore = null) {
    	$fields = '*';
    	if (!$full)
    		$fields = $this->some_fields;

		$query = $this->db->select($fields)
			->from($this->tbl_users)
			->where('id', $userID)
			->get();
		$result = $query->row_array();
		if (isset($result['level']))
			$result['level'] = $this->levelToAPI($result['level']);
		$this->photoToApi($result, $filePath, $filePathStore);

		//make super sure no pass is returned
		unset($result['pass']);
		unset($result['id']);
		$result['last_login'] = $this->formatOutputDate($result['last_login']);
		if (empty($result['show_phone']))
			$result['phone'] = "";

		return $result;
	}

	public function getAllPairs() {
        $query = $this->db->select($this->min_fields)
            ->from($this->tbl_users)
            ->get();
        $result = $query->result_array();
        $out = array();
        foreach ($result as $key => $item) {
            $out[$result[$key]['id']] = $result[$key]['firstname'] . ' ' . $result[$key]['lastname'];
        }
        return $out;
    }

	public function getUserMatches($userID): array {
		$query = $this->db->from($this->tbl_users_matches)
			->where('user_id', $userID)
			->count_all_results();
		$return['gamesPlayed'] = $query;
		$return['gamesWon'] = 0;

		$query = $this->db->select('m.winner, um.team, m.score_added, m.match_dtime')
			->from($this->tbl_users_matches.' um')
			->where('um.user_id', $userID)
			->where('um.team=m.winner')
			->join($this->tbl_matches.' m', 'm.id = um.match_id', 'left')
			->order_by('m.last_edit', 'DESC')
			->get();

		$return['lastGameDate'] = '';
		foreach ($query->result_array() as $row)
			if ($row['winner'] != '' && strtotime($row['score_added']) > 0) {
				if(empty($return['lastGameDate']))
					$return['lastGameDate'] = $this->formatOutputDate($row['match_dtime']);
				$return['gamesWon']++;
				}
		return $return;
	}

	public function getUserRank($userID): array {
		$query = $this->db->select('AVG(u_character) AS avgc, AVG(u_technique) AS avgt')
			->from($this->tbl_reviews)
			->where('user_id', $userID)
			->get();

		$return = array();
		$return['character'] = $query->row_array()['avgc'];
		$return['technique'] = $query->row_array()['avgt'];
		return $return;
	}

	public function updateLastLogin($userID = null) {
    	if (empty($userID)) {
			$this->load->library('session');
			return $this->db->where('id', $this->session->user['id'])->update($this->tbl_users, array('last_login' => $this->nowToDatestamp()));
		}
	}

	/**
	 * @param $input
	 * @return string
	 */
	private function uPassword($input):string {
		return password_hash($input, PASSWORD_DEFAULT, array('cost' => $this->p_cost));
	}

    /**
     * @param bool $short
     * @param string $pin_list
     * @return string
     */
    public function generatePin($short = false, $pin_list = array()) {
		$return = base_convert(microtime(false), 10, 36);
		if ($short)
			$return = substr($return, 3, 8);
		while (in_array($return, $pin_list)) {
			print 'running';
			$return = base_convert(microtime(false), 10, 36);
			if ($short)
				$return = substr($return, 3, 8);
		}
        return $return;
    }

    /**
     * @param $pin
     * @param $pin2
     * @param bool $extended
     * @param int $id
     * @return bool
     */
    public function checkKey($id = 0, $pin, $pin2 = false, $extended = false) {

    	if (ENVIRONMENT == 'development' && $pin == '123')
    		return true;
        $id = (int)trim($id);
        if ($pin2 == false) {
            $pin2 = $this->getKey($id);
        }
        if (empty($pin) | empty($pin2))
            return false;
        if ($extended && $id > 0)
            $pin2 = substr_replace(substr_replace($pin2, $this->getMail($id)[$id%5], ($id%5), 0), chr(60+($id%65)), (5+$id%5), 0);
        if ($pin == $pin2) {
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return null
     */
    public function getKey($id) {
        $query = $this->db->select('key')
            ->from($this->tbl_users)
            ->where('id', (int) $id)
            ->get();

        $result = $query->row();
        if(is_object($result))
            return $result->key;
        else
            return null;
    }

    public function levelToAPI($level) {
		return $level;
	}

	public function levelFromAPI($level):int {
    	return $level;

	}

	public function photoToApi(&$user, $filePath, $filePathStore) {
        if (empty($user))
            return false;
        if (isset($user['photo']) && (file_exists($filePathStore . $user['photo']) || true))
            $user['photo'] = $filePath . $user['photo'] . '?t=' . time();
        else
            $user['photo'] = '';
    }

	/**
	 * check ALL below:
	 */
	public function checkFriends($id1,$id2) {
		$result = $this->db->from($this->tbl_user_user)
			->where(array('id_user_1' => $id1, 'id_user_2' => $id2))
			->count_all_results();

		if ($result == 1)
			return true;
		else
			return false;
	}

	public function addFriend($id1, $id2) {
		return $this->db->insert($this->tbl_user_user, array('id_user_1' => $id1, 'id_user_2' => $id2));
	}

	public function removeFriend($id1, $id2) {
		return $this->db->delete($this->tbl_user_user, array('id_user_1' => $id1, 'id_user_2' => $id2));
	}

	public function confirmFriend($id1, $id2) {
		return $this->db->where(array('id_user_1' => $id1, 'id_user_2' => $id2))
			->update($this->tbl_user_user, array('accepted' => 1));
	}

	public function updateLocation($id, $lat, $lng){
		return $this->db->replace('users_geo', array('id_user' => $id, 'latitude' => $lat, 'longitude' => $lng));
	}

	public function getNearbyUsers($id, $maxDistance, $limit=20)
	{
		$query = $this->db->select(array('latitude', 'longitude'))->from('users_geo')->where('id_user', $id)->get();
		$location = $query->row();

		$this->pdo = $this->_loadDatabase($this->config->item('DB_ACTIVE_GROUP'));
		$stmt = $this->pdo->prepare("
			SELECT id_user, title, firstname, lastname, phone_name, email, job_title, latitude, longitude, 
			( 6371 * acos(cos(radians(:latitude))*cos(radians(latitude))*cos(radians(longitude) - radians(:longitude))+sin(radians(:latitude)) * sin( radians(latitude) ) ) ) AS distance
			FROM users_geo g JOIN users u ON g.id_user=u.id
			HAVING distance <= :maxDistance AND id_user != :id
			ORDER BY distance ASC
			LIMIT ".$limit);
		$stmt->bindParam(':maxDistance', $maxDistance);
		$stmt->bindParam(':latitude', $location->latitude);
		$stmt->bindParam(':longitude', $location->longitude);
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		return $stmt->fetchAll();
	}

	public function getRequests($id) {
		$this->pdo = $this->_loadDatabase($this->config->item('DB_ACTIVE_GROUP'));
		$stmt = $this->pdo->prepare("
			SELECT uu.id_user_1 AS id_user, uu.time_added, u.firstname, u.lastname, u.job_title, 
			UNIX_TIMESTAMP(uu.time_added) AS time_added_s
			FROM user_user uu 
			LEFT JOIN users u 
			ON uu.id_user_1 = u.id 
			WHERE uu.id_user_2=:uid AND 
			uu.time_added > (SELECT NOW() - INTERVAL 2 HOUR) 
			ORDER BY uu.time_added DESC");//@todo change this to 1 hr in prod
		$stmt->bindParam(':uid', $id);
		$stmt->execute();
		return $stmt->fetchAll();
	}

	public function getOwnContacts($id, $search_string) {
		$query = $this->db->select(array('id', 'CONCAT(firstname, \' \', lastname) AS name', 'email', 'phone1 AS phone'))
			->from($this->tbl_user_user)
			->join($this->tbl_users, $this->tbl_user_user.'.id_user_1 = '.$this->tbl_users.'.id')
			->where('id_user_2', $id)
			->group_start()
			->or_like(array('firstname' => $search_string, 'lastname' => $search_string, 'job_title' => $search_string,
				'email' => $search_string, 'company' => $search_string))
			->group_end()
			->order_by($this->tbl_users.'.lastname', 'ASC')
			->get();
		return $query->result_array();
	}

}

