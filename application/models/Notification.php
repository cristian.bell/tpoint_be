<?php
class Notification extends MY_Model {

    protected $main_tbl = 'notifications';
    protected $main_tbl_short = ' t';
    protected $linked_tbl = 'users_notifications';
    protected $linked_tbl_short = ' un';
    protected $all_fields = 't.*';
    protected $min_fields = 'id, src, dst, title, message, redirected, created, was_read';
    //protected $unread_only_cond = "t.id NOT IN (SELECT notification_id FROM users_notifications WHERE user_id = '";
    //protected $unread_only_cond = "un.read_on = ''"; //todo remove this
    protected $def_cond = "(t.dst = '*' OR t.dst = '";

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * creates a system to user notification
     */
    public function addSystem(string $what, string $dst = '*', string $id = null):bool {
        $data = array('src' => 0);
        switch ($what) {
            case 'tournament_new':
                if (empty($id))
                    return false;
                $data['dst'] = '*';
                $data['title'] = 'A new tournament has been created!';
                $data['message'] = 'Check out the new tournament here: ';
                $data['redirect'] = 'home/TournamentView/' . $id;
                break;
        }
        return $this->db->insert($this->main_tbl, $data);

    }

    /**
     * creates a user to user notification
     */
    public function addUser() {

    }

    public function read(string $userID, int $id) {
        if (empty($userID) || empty($id))
            $success = false;
        else {
            $data = array('user_id' => $userID, 'notification_id' => $id);
            $success = $this->db->insert($this->linked_tbl, $data);
        }
        return array('success' => $success);
    }

    public function getAll(string $userID, bool $unread_only = false) {
        $fields = $this->all_fields;
        $this->load->model('User');
        $users = $this->User->getAllPairs();

        $query = $this->db->select($fields . ', un.read_on')
            ->from($this->main_tbl . $this->main_tbl_short)
            ->where($this->def_cond . $userID . '\')')
            ->join($this->linked_tbl . $this->linked_tbl_short, 't.id = un.notification_id', 'left')
            ->order_by('t.created', 'DESC');

        $result = $query->get()->result_array();

        foreach ($result as $key => $item) {
            if ($unread_only && !empty($result[$key]['read_on']))
                unset($result[$key]);
            else {
                $result[$key]['created'] = $this->formatOutputDate($item['created']);
                if ($result[$key]['src'] ==  '0')
                    $result[$key]['src_name'] = 'System';
                else
                    if (key_exists($result[$key]['src'], $users)) {
                        $result[$key]['src_name'] = $users[$result[$key]['src']];
                    }
                    else
                        $result[$key]['src_name'] = 'Some user';
            }
        }

        return array_values($result);
    }

    public function get(string $userID, string $id) {
        $fields = $this->all_fields;

        $query = $this->db->select($fields . ', un.read_on')
            ->from($this->main_tbl . $this->main_tbl_short)
            ->where($this->def_cond . $userID . '\')')
            ->join($this->linked_tbl . $this->linked_tbl_short, 't.id = un.notification_id', 'left')
            ->order_by('t.created', 'DESC');

        $query->where('t.id = ' . $id);

        $result = $query->get()->result_array();
        $result['created'] = $this->formatOutputDate($result['created']);
        if ($result['src'] ==  '0')
            $result['src_name'] = 'System';

        return $result;
    }


}


