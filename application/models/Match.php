<?php
class Match extends MY_Model {

	protected $tbl_matches = 'matches';
	protected $tbl_users_matches = 'users_matches';
	protected $tbl_tournaments = 'tournaments';
    protected $tbl_users = 'users';
	protected $all_fields = 'm.match_dtime as dtime, m.id, m.location, m.winner, m.score, m.last_edit, m.score_added, m.score_conf, tournament_id, score_added_by';
	protected $min_fields = 'id, location, match_dtime as dtime, tournament_id';
	protected $cond_future = 'm.match_dtime > NOW() OR m.match_dtime IS NULL';

	public function __construct()
	{
		parent::__construct();
	}

	public function add($data) {
		$data['match_dtime'] = $this->formatInputDate($data['match_dtime'], true);

		$return = false;
		$success = $this->db->insert($this->tbl_matches, $data);
		if ($success)
			$return = $this->db->insert_id();
		return $return;
	}

    public function edit($data, $id) {
		if (!empty($data['score']) && empty($data['score_added'])) {
			$data['score_added'] = $this->formatInputDate(null, true);
		}
        $this->db->where('id', $id);
        $success = $this->db->update($this->tbl_matches, $data);

        if($success)
            return $success;
        else
            return false;
    }

    public function computeWinner($id) {

    }

    public function isDisputable($id, $userID) {
	    $match = $this->getByID($id);
	    if (!empty($match['score']) && $match['score_conf'] === '1')
	        return false;
	    $matchUsers = $this->getMatchUsers($id, true);
	    foreach ($matchUsers as $matchUser) {
	        if ($matchUser['user_id'] === $userID)
	            return true;
        }
	    return false;
    }


	public function getAll($futureOnly = false, $full = true) {
        $this->load->model('Tournament');

		$fields = $this->all_fields;
		if (!$full || $futureOnly)
			$fields = $this->min_fields;

		$query = $this->db->select($fields)
			->from($this->tbl_matches.' m')
			->order_by('m.match_dtime', 'DESC');
		if ($futureOnly)
			$query->where($this->cond_future);

		$result = $query->get()->result_array();
		$this->prepOutputData($result, $this->Tournament->getAllPairs());

		return $result;
	}

	public function getAllByUser($userID, $futureOnly = false, $full = true) {
        $this->load->model('Tournament');

		$fields = $this->all_fields;
		if (!$full || $futureOnly)
			$fields = $this->min_fields;

		$query = $this->db->select($fields)
			->from($this->tbl_users_matches.' um')
			->where('um.user_id', $userID)
			->join($this->tbl_matches.' m', 'm.id = um.match_id', 'left')
			->order_by('m.match_dtime', 'DESC');
        if ($futureOnly)
			$query->where($this->cond_future);

		$result = $query->get()->result_array();

        $this->prepOutputData($result, $this->Tournament->getAllPairs(), $userID);

		return $result;
	}

	public function getByID($id, $full = true) {
		$fields = $this->all_fields;
		if (!$full)
			$fields = $this->min_fields;

		$query = $this->db->select($fields)
			->from($this->tbl_matches.' m')
			->where('id', $id)
			->get();
		$result = $query->row_array();
		$result['dtime'] = $this->formatOutputDate($result['dtime'], true);
		if (isset($result['last_edit']))
            $result['last_edit'] = $this->formatOutputDate($result['last_edit'], true);

		return $result;
	}

	public function addMatchUsers($data, $matchID):bool {
    	$success = true;
    	$double = parent::$userIDLen*2 + 1;

		if (strlen($data['user_id1']) == $double && strpos($data['user_id1'], ';') == parent::$userIDLen && strlen($data['user_id2']) == $double && strpos($data['user_id2'], ';') == parent::$userIDLen) {
			$data['user_id1'] = explode(';', $data['user_id1']);
			$data['user_id2'] = explode(';', $data['user_id2']);
			foreach ($data['user_id1'] as $userID) {
				$success *= $this->db->insert($this->tbl_users_matches, array('user_id' => $userID, 'match_id' => $matchID, 'team' => 1));
			}
			foreach ($data['user_id2'] as $userID) {
				$success *= $this->db->insert($this->tbl_users_matches, array('user_id' => $userID, 'match_id' => $matchID, 'team' => 2));
			}
		} elseif (strlen($data['user_id1']) == parent::$userIDLen && strlen($data['user_id2']) == parent::$userIDLen) {
			$success *= $this->db->insert($this->tbl_users_matches, array('user_id' => $data['user_id1'], 'match_id' => $matchID, 'team' => 1));
			$success *= $this->db->insert($this->tbl_users_matches, array('user_id' => $data['user_id2'], 'match_id' => $matchID, 'team' => 2));
		}
		return $success;
	}

	public function getMatchUsers($matchID, $full = false) {
    	$fields = 'user_id, team';
    	$return = null;
    	if ($full)
    		$fields = '*';
		$query = $this->db->select($fields)
			->from($this->tbl_users_matches.'')
			->where('match_id', $matchID)
			->get();
		$result = $query->result_array();
		if (!$full)
			foreach ($result as $item) {
				$return[] = $item['user_id'];
			}
		else
			$return = $result;
		return $return;
	}

	public function deleteMatchUsers($matchID) {
		$this->db->delete($this->tbl_users_matches, array('match_id' => $matchID));
	}

	private function prepOutputData(&$result, $tournaments, $userID = null) {
        $matchIDs = '0';

        foreach ($result as $key => $item) {
            $result[$key]['dtime'] = $this->formatOutputDate($item['dtime']);
            $result[$key]['t_name'] = $tournaments[$result[$key]['tournament_id']]['name'];
            $result[$key]['t_start'] = $this->formatOutputDateMonth($tournaments[$result[$key]['tournament_id']]['start']);
            $result[$key]['t_end'] = $this->formatOutputDateMonth($tournaments[$result[$key]['tournament_id']]['end']);
            $result[$key]['t_location'] = $tournaments[$result[$key]['tournament_id']]['location'];
            $matchIDs .= ', ' . $result[$key]['id'];
        }

        if (!empty($userID)) {
            $query = $this->db->select('um.user_id, u.firstname, u.lastname, match_id')
                ->from($this->tbl_users_matches.' um')
                ->where('um.user_id !=', $userID)
                ->where('match_id IN ('. $matchIDs . ')')
                ->join($this->tbl_users.' u', 'um.user_id = u.id', 'left')
                ->group_by('match_id');
            $opponents = $query->get()->result_array();

            $opponentList = array();
            foreach ($opponents as $i => $opponent) {
                $opponentList[$opponent['match_id']] = $opponents[$i];
            }

            foreach ($result as $key => $item) {
                if (!empty($opponentList[$result[$key]['id']])) {
                    $result[$key]['o_id'] = $opponentList[$result[$key]['id']]['user_id'];
                    $result[$key]['o_name'] = $opponentList[$result[$key]['id']]['firstname'] . ' ' . $opponentList[$result[$key]['id']]['lastname'];
                }
            }
        }
    }

}

