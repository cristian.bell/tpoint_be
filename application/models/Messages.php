<?php 
class Messages extends MY_Model {

    protected $tbl = 'users_messages';

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * saves the current message
     * @return mixed
     */
	public function addMessage($data) {
		return $this->db->insert($this->tbl, $data);
	}

	/**
	 * returns all messages
	 * @return mixed
	 */
	public function getAll() {
		$query = $this->db->select('*')
			->from($this->tbl)
			->get();
		return $query->result_array();
	}

}

