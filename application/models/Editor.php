<?php 
class Editor extends CI_Model{

    public $tbl_tpl = 'templates';
    public $tbl_fonts = 'fonts';
    public $tbl_tpl_item = "templates_items";

    /**
     * * return all enabled fonts as an array. If keyValuePair is true, an array of ID->Value will be returned.
     * @param bool $keyValuePair
     * @return array
     */
    public function getEnabledFonts($keyValuePair = false) {
        $query = $this->db->select('*')
            ->from($this->tbl_fonts)
            ->get();

        $result = [];
        if($keyValuePair) {
            foreach($query->result_array() as $item) {
                $result[$item['id']] = $item['name'];
            }
        } else
            $result = $query->result_array();
        return $result;
    }

    /**
     * saves the current template info
     * @return mixed
     */
    public function addTemplate($data) {
        //var_dump($data);
        $success = $this->db->insert($this->tbl_tpl, $data);
        if($success)
            return $this->db->insert_id();
        else
            return false;
    }

    /**
     * edits template information
     * @param $data
     * @param $id
     * @return bool
     */
    public function editTemplate($data, $id) {
        $this->db->where('id', $id);
        $success = $this->db->update($this->tbl_tpl, $data);
        if($success)
            return $success;
        else
            return false;
    }

    /**
     * @param $id
     * @return null
     */
    public function getTemplateInfo($id) {
        $query = $this->db->select('*')
            ->from($this->tbl_tpl)
            ->where('id', $id)
            ->get();
        $result = $query->row();
        if(is_object($result))
            return $result;
        else
            return null;
    }

    public function getTemplateItems($id, $onlyRelevant = true) {
        if($onlyRelevant) {
            $columns = "item, x, y, x_rel, y_rel, align_parent, font_name, font_size, color, bold, italic ";
        } else
            $columns = "*";
        $query = $this->db->select($columns)
            ->from($this->tbl_tpl_item)
            ->where('id_template', $id)
            ->get();
        return $query->result_array();
    }

    public function getTemplateItem($id, $item) {
        $query = $this->db->select('*')
            ->from($this->tbl_tpl_item)
            ->where(array('id_template' => $id, 'item' => $item))
            ->get();
        return $query->result_array();
    }

    public function listTemplates() {
        $query = $this->db->select('id, time_edit, name')
            ->from($this->tbl_tpl)
            ->get();
        foreach($query->result_array() as $item) {
            $result[$item['id']] = [(int)$item['time_edit'], $item['name']];
        }
        return $result;
    }

    public function listTemplatesTimeEdit() {
        $query = $this->db->select('id, time_edit')
            ->from($this->tbl_tpl)
            ->get();
        foreach($query->result_array() as $item) {
            $result[$item['id']] = (int)($item['time_edit']);
        }
        return $result;
    }

    /**
     * return all templates as an array. If keyValuePair is true, an array of ID->Value will be returned.
     * @param bool $keyValuePair
     * @return array
     */
    public function getAllTemplates($keyValuePair = false) {
        $query = $this->db->select('*')
            ->from($this->tbl_tpl)
            ->get();

        $result = [];
        if($keyValuePair) {
            foreach($query->result_array() as $item) {
                $result[$item['id']] = urldecode($item['name']);
            }
        } else
            $result = $query->result_array();
        return $result;
    }

    /**
     * @param $data
     * @param $id
     * @return bool
     */
    public function addTemplateItem($data, $id) {
        $data['id_template'] = $id;
        $success = $this->db->insert($this->tbl_tpl_item, $data);
        if($success)
            return $this->db->insert_id();
        else
            return false;
    }

    /**
     * @param $item
     * @param $id
     * @return mixed
     */
    public function deleteTemplateItem($item, $id) {
        return $this->db->delete($this->tbl_tpl_item, array('item' => $item, 'id_template'=> $id));
    }

    /**
     * @param $data
     * @param $item
     * @param $id
     * @return bool
     */
    public function editTemplateItem($data, $item, $id) {
        $this->db->where(array('item' => $item, 'id_template' => $id));
        $success = $this->db->update($this->tbl_tpl_item, $data);
        if($success)
            return $success;
        else
            return false;
    }

}

