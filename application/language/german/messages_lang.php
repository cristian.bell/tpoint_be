<?php
$lang['200'] = "&Auml;nderungen erfolgreich gespeichert.";
$lang['400'] = "Keine Verbingund zum Server m&ouml;glich.";
$lang['403'] = "Zugang verweigert - ist die App aktuell?";
$lang['404'] = "Keine Verbingund zum Server m&ouml;glich.";

$lang['new_account_mail_body'] = "Konto erfolgreich erstellt.";
$lang['TXT_NO_SAVE'] = "Keine &Auml;nderungen.";
$lang['TXT_SAVE_OK'] = "Eintrag erfolgreich gespeichert!";
$lang['TXT_SAVE_NOK'] = "Etwas ist leider schief gelaufen. Bitte versuchen Sie erneut.";