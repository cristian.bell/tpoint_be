<?php
$lang['LBL_SAVE'] = "Speichern";
$lang['LBL_NEW'] = "Neues Element";
$lang['LBL_CANCEL'] = "Abbrechen";

$lang['ED_TITLE_BLUR'] = "Geben Sie eine Name f&uml;er das Template.";
$lang['ED_ME_ITEM_OVER'] = "Click to add me to the card!";
$lang['ED_CA_ITEM_OVER'] = "Select and press delete to remove from the template";
$lang['ED_CA_ITEM_MOVE'] = "Drag the element to the desired position";

$lang['ED_ME_SIZE_LBL'] = "Schriftgröße";
$lang['ED_ME_FONT_LBL'] = "Schriftart";
$lang['ED_ME_COLOR_LBL'] = "Farbe";
$lang['ED_ME_BOLD_LBL'] = "<b>F</b>";
$lang['ED_ME_ITALIC_LBL'] = "<i>K</i>";