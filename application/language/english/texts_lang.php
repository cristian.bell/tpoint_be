<?php
$lang['LBL_SAVE'] = "Save";
$lang['LBL_NEW'] = "New";
$lang['LBL_CANCEL'] = "Cancel";

$lang['ED_TITLE_BLUR'] = "Enter a title for the template";
$lang['ED_ME_ITEM_OVER'] = "Click to add me to the card!";
$lang['ED_CA_ITEM_OVER'] = "Select and press delete to remove from the template";
$lang['ED_CA_ITEM_MOVE'] = "Drag the element to the desired position";

$lang['ED_ME_SIZE_LBL'] = "Text size";
$lang['ED_ME_FONT_LBL'] = "Font name";
$lang['ED_ME_COLOR_LBL'] = "Text color";
$lang['ED_ME_BOLD_LBL'] = "<b>B</b>";
$lang['ED_ME_ITALIC_LBL'] = "<i>i</i>";