<?php
$lang['200'] = "All went ok";
$lang['400'] = "Error connecting to the server";
$lang['403'] = "Forbidden - is your app up to date?";
$lang['404'] = "Error connecting to the server";

$lang['new_account_mail_body'] = "account created, you are the greatest";
$lang['TXT_NO_SAVE'] = "No changes to the data were made.";
$lang['TXT_SAVE_OK'] = "Changes successfully saved.";
$lang['TXT_SAVE_NOK'] = "Something went wrong and the changes were not saved. Please try again later.";