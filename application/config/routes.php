<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['user/me']['get'] = 'Usercontroller/me';
$route['user/login']['post'] = 'Usercontroller/login';
$route['user/login']['options'] = 'Usercontroller/login';
$route['user/relogin']['post'] = 'Usercontroller/relogin'; // todo implement me!
$route['user/relogin']['options'] = 'Usercontroller/relogin';
$route['user/logout']['post'] = 'Usercontroller/logout';
$route['user/logout']['options'] = 'Usercontroller/logout';

$route['user/([a-z0-9]+)']['get'] = 'Usercontroller/get/$1';
$route['user/([a-z\_]+)']['get'] = 'Usercontroller/$1'; //todo check this route.
$route['user/save']['options'] = 'Usercontroller/save';
$route['user/save']['post'] = 'Usercontroller/save';
$route['user/save']['patch'] = 'Usercontroller/save';

$route['user/photo']['options'] = 'Usercontroller/photo';
$route['user/photo']['patch'] = 'Usercontroller/photo';

$route['match']['post'] = 'Matchcontroller/save';
$route['match']['patch'] = 'Matchcontroller/save';
$route['match/([0-9]+)']['get'] = 'Matchcontroller/get/$1';
$route['match/confirm/([0-9]+)']['post'] = 'Matchcontroller/confirm/$1';
$route['match/dispute/([0-9]+)']['post'] = 'Matchcontroller/dispute/$1';
$route['match/list']['get'] = 'Matchcontroller/listAll';
$route['match/list/future']['get'] = 'Matchcontroller/listAllFuture';
$route['match/list/([a-z0-9]+)']['get'] = 'Matchcontroller/listAllByUser/$1';
$route['match/list/future/([a-z0-9]+)']['get'] = 'Matchcontroller/listAllFutureByUser/$1';
$route['match/mine']['get'] = 'Matchcontroller/listAllByUser';
$route['match/mine/future']['get'] = 'Matchcontroller/listAllFutureByUser';

$route['review']['post'] = 'Reviewcontroller/save';
$route['review/([a-z0-9]+)/([0-9]+)']['get'] = 'Reviewcontroller/get/$1/$2';
$route['reviews/([a-z0-9]+)']['get'] = 'Reviewcontroller/getAll/$1';
$route['reviews/mine']['get'] = 'Reviewcontroller/getAll/';

$route['tournament']['post'] = 'Tournamentcontroller/save';
$route['tournament']['patch'] = 'Tournamentcontroller/save';
$route['tournament/([0-9]+)']['delete'] = 'Tournamentcontroller/deactivate/$1';
$route['tournament/([0-9]+)']['get'] = 'Tournamentcontroller/get/$1';
$route['tournament/([0-9]+)']['options'] = 'Tournamentcontroller/deactivate/$1';
$route['tournament/join/([0-9]+)']['post'] = 'Tournamentcontroller/join/$1';
$route['tournament/leave/([0-9]+)']['post'] = 'Tournamentcontroller/leave/$1';
$route['tournament/list']['get'] = 'Tournamentcontroller/listAll';
$route['tournament/list/future']['get'] = 'Tournamentcontroller/listAllFuture';
$route['tournament/by_user/([a-z0-9]+)']['get'] = 'Tournamentcontroller/listAllByUser/$1';
$route['tournament/by_user/future/([a-z0-9]+)']['get'] = 'Tournamentcontroller/listAllFutureByUser/$1';
$route['tournament/start/([a-z0-9]+)']['get'] = 'Tournamentcontroller/start/$1';

$route['notification/list']['get'] = 'Notificationcontroller/listAll';
$route['notification/list/unread']['get'] = 'Notificationcontroller/listAllUnread';
$route['notification/read/([0-9]+)']['post'] = 'Notificationcontroller/read/$1';
$route['notification/([0-9]+)']['get'] = 'Notificationcontroller/get/$1';

//var_dump($_SERVER);

//$route['test']['get'] = 'Usercontroller/test';

/*

$route['card/(.*)'] = 'Cardcontroller/$1';
$route['editor/([a-zA-Z]+)(/\d+)*'] = 'Editorcontroller/$1$2';
$route['editor(/){0,1}'] = 'Editorcontroller';
$route['messages/(/\d+)*'] = 'Messagescontroller/$1';
$route['messages(/){0,1}'] = 'Messagescontroller';
$route['settings/([a-zA-Z]+)(/(\d+|[a-z]+))*'] = 'Settingscontroller/$1$2';*/
