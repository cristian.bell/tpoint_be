<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout {

 var $append_views = array();
 var $callback = '';
 var $data = array();
 var $prepend_views = array();

 function __construct(){

  $this->ci =& get_instance();

 }

 function append($view, $data = null){

  if (!empty($view)){

   $this->append_views[] = $view; 
   $this->set($data); 

  }

 }

 function build($view, $data = null, $no_append = false){

  $html = '';

  if (!empty($view)){

   $this->set($data); 

   if (!$no_append){

    if (count($this->prepend_views)){

     foreach ($this->prepend_views as $prepend_view){

      $html .= $this->ci->load->view($prepend_view, $this->data, true); 

     }

    }

   }

   $html .= $this->ci->load->view($view, $this->data, true);

   if (!$no_append){

    if (count($this->append_views)){

     foreach ($this->append_views as $append_view){

      $html .= $this->ci->load->view($append_view, $this->data, true); 

     }

    }

   }

   if (!empty($this->callback)){

    $html = call_user_func($this->callback, $html); 

   }

  }

  echo $html;

 }

 function get(){

  return $this->data;

 }

 function prepend($view, $data = null){

  if (!empty($view)){

   $this->prepend_views[] = $view; 
   $this->set($data); 

  }


 }

 function set($data){

  if (is_array($data)){

   $this->data = array_merge($this->data, $data); 

  }

 }

 function set_callback($callback){

  if (is_callable($callback)){

   $this->callback = $callback;

  }

 }

}

/* End of file layout.php */
/* Location: ./application/libraries/layout.php */